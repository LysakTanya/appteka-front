import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import IStorageInfo from "./data/IStorageInfo";
import storageService from "./data/storageService";
import moment from "moment";
import { Tab, Tabs } from "react-bootstrap";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import Table from "../components/Table";

const StoragePage = () => {
  let { id }: any = useParams();
  const [storage, setStorage] = useState<IStorageInfo>({} as IStorageInfo);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchStorage = async () => {
      if (id == null || id == 0) {
        id = Number(localStorage.getItem("storageId"));
      }

      try {
        setLoading(true);
        let response = await storageService.getStorageInfoById(id);
        setStorage(response.data);
        setLoading(false);
      } catch (error) {
        toast.error(error.response.data);
        setLoading(false);
      }
    };

    fetchStorage();
  }, []);

  const workerColumns: any = [
    {
      name: "Ім'я",
      selector: "name",
      sortable: true,
    },
    {
      name: "Прізвище",
      selector: "surname",
      sortable: true,
    },
    {
      name: "Дата початку роботи",
      selector: "hireDate",
      sortable: true,
      cell: (row: any) => {
        return <div>{moment(row.hireDate).format("MMM DD, YYYY")}</div>;
      },
    },
    {
      name: "Дата народження",
      selector: "birthDate",
      sortable: false,
      cell: (row: any) => {
        return <div>{moment(row.birthDate).format("MMM DD, YYYY")}</div>;
      },
    },
    {
      name: "Роль",
      selector: "type",
      sortable: false,
      cell: (row: any) => {
        return <div>{row.type.name}</div>;
      },
    },
  ];

  const medicineColumns: any = [
    {
      name: "Код",
      selector: "medicine.code",
      sortable: false,
    },
    {
      name: "Назва",
      selector: "medicine.name",
      sortable: true,
    },
    {
      name: "Ціна",
      selector: "medicine.price",
      sortable: true,
    },
    {
      name: "Категорія",
      selector: "medicine.category.name",
      sortable: true,
    },
    {
      name: "Кількість/од",
      selector: "medicine.quantity",
      sortable: true,
      cell: (row: any) => {
        return (
          <div>
            {row.medicine.quantity} {row.medicine.category.measure}
          </div>
        );
      },
    },
    {
      name: "Кількість на складі",
      selector: "count",
      sortable: true,
      cell: (row: any) => {
        return <div>{row.count} шт.</div>;
      },
    },
  ];

  return (
    <>
      <h2>Торгова точка №{id ?? localStorage.getItem("storageId")}</h2>
      <p>
        <b>Адреса: </b> м. {storage.city}, вул. {storage.street}{" "}
        {storage.number}
      </p>
      <Tabs defaultActiveKey="workers">
        <Tab eventKey="workers" title="Працівники">
          <Table
            columns={workerColumns}
            title={"Працівники торгової точки"}
            data={storage?.workers}
            loading={loading}
          />
        </Tab>
        <Tab eventKey="medicines" title="Препарати">
          <Table
            columns={medicineColumns}
            title={"Препарати торгової точки"}
            data={storage?.medicines}
            loading={loading}
          />
        </Tab>
      </Tabs>
      <Toast />
    </>
  );
};

export default StoragePage;
