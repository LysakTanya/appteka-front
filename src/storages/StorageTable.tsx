import IStorage from "./data/IStorage";
import { BiEditAlt } from "react-icons/bi";
import Table from "../components/Table";
import StorageModal from "./StorageModal";
import { useState } from "react";
import { toast } from "react-toastify";
import storageService from "./data/storageService";
import Toast from "../components/Toast";

interface IStorageTableProps {
  data: IStorage[];
  loading: boolean;
}

const StorageTable = (props: IStorageTableProps) => {
  const { data } = props;
  const [visible, setVisible] = useState(false);
  const [activeStorage, setStorage] = useState<IStorage>({} as IStorage);
  const handleClose = () => {
    setVisible(false);
  };

  const handleEditClick = (row: any) => {
    setStorage(row);
    setVisible(true);
  };

  const columns = [
    {
      name: "Номер складу",
      selector: "id",
      sortable: true,
    },
    {
      name: "Місто",
      selector: "city",
      sortable: true,
    },
    {
      name: "Назва",
      selector: "street",
      sortable: true,
    },
    {
      name: "Номер будівлі",
      selector: "number",
      sortable: false,
    },
    {
      name: "",
      selector: "id",
      sortable: false,
      right: true,
      button: true,
      cell: (row: any) => (
        <>
          <BiEditAlt
            size="1.7rem"
            style={{ margin: 4 }}
            onClick={() => handleEditClick(row)}
          />
        </>
      ),
    },
  ];

  const handleUpdateStorage = async (values: any) => {
    const storage: IStorage = {
      id: values.id,
      city: values.city,
      street: values.street,
      number: values.number,
    };

    try {
      let response = await storageService.updateStorage(storage);
      toast.success("Торогова точка була оновлена.");
      handleClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <Table
        columns={columns}
        data={data}
        loading={props.loading}
        title={"Торгові точки"}
        onRowClicked={(row: any) => {
          window.location.href = `/storages/${row.id}`;
        }}
      />
      <StorageModal
        initialValue={activeStorage}
        handleClose={handleClose}
        visible={visible}
        onSubmit={handleUpdateStorage}
        title="Оновити торгову точку"
      />
      <Toast />
    </>
  );
};

export default StorageTable;
