import ICategory from "../../medicines/data/ICategory";
import IMedicine from "../../medicines/data/IMedicine";

interface IStorageMedicine {
  medicine: IMedicine;
  count: number;
}

export default IStorageMedicine;
