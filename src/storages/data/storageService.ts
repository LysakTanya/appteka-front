import Axios from "../../Axios";
import Routes from "../../Routes";
import IStorage from "./IStorage";
import IStorageInfo from "./IStorageInfo";
import IStorageMedicine from "./IStorageMedicine";

const storageService = {
  getAllStorages: () => {
    return Axios.get<IStorage[]>(Routes.storageRoute);
  },

  getStorageInfoById: (id: number) => {
    return Axios.get<IStorageInfo>(Routes.storageRoute + id);
  },

  addStorage: (storage: IStorage) => {
    return Axios.post<IStorage>(Routes.storageRoute, storage);
  },

  updateStorage: (storage: IStorage) => {
    return Axios.put<IStorage>(Routes.storageRoute, storage);
  },

  getStorageMedicines: async () => {
    return Axios.get<IStorageMedicine[]>(`${Routes.storageRoute}medicines`);
  },
};

export default storageService;
