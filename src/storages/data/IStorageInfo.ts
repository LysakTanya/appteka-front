import IWorker from "../../workers/data/IWorker";
import IStorageMedicine from "./IStorageMedicine";

interface IStorageInfo {
  id: number;
  workers: IWorker[];
  medicines: IStorageMedicine[];
  city: string;
  street: string;
  number: number;
}

export default IStorageInfo;
