import { StringLiteralLike } from "typescript";

interface IStorage {
    id: number;
    city: string;
    street: string;
    number: number;
}

export default IStorage;