import { useState, useEffect } from "react";
import StorageTable from "./StorageTable";
import IStorage from "./data/IStorage";
import storageService from "./data/storageService";
import AddButton from "../components/AddButton";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import StorageModal from "./StorageModal";

const AdminStoragePage = () => {
  const [storages, setStorages] = useState<IStorage[]>([] as IStorage[]);
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleClose = () => {
    setVisible(false);
  };
  useEffect(() => {
    const fetchStorages = async () => {
      try {
        setLoading(true);
        let response = await storageService.getAllStorages();
        setStorages(response.data);
        setLoading(false);
      } catch (error) {
        toast.error(error.response.data);
        setLoading(false);
      }
    };

    fetchStorages();
  }, []);

  const handleAddStorage = async (values: any) => {
    const storage: IStorage = {
      id: values.id,
      city: values.city,
      street: values.street,
      number: values.number,
    };

    try {
      let response = await storageService.addStorage(storage);
      toast.success("Нова торгова точка була успішно створена.");
      handleClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <div>
      <AddButton onClick={() => setVisible(true)} />
      <StorageTable data={storages} loading={loading} />
      <StorageModal
        initialValue={{} as IStorage}
        handleClose={handleClose}
        visible={visible}
        onSubmit={handleAddStorage}
        title="Нова торгова точка"
      />
      <Toast />
    </div>
  );
};

export default AdminStoragePage;
