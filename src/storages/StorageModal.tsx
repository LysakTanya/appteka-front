import { Modal, Form, Button, Col } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { FormikErrors, FormikState, useFormik } from "formik";
import { useEffect } from "react";
import { toast } from "react-toastify";
import IStorage from "./data/IStorage";


interface IStorageModalProps {
  onSubmit: (values: any) => Promise<void>;
  handleClose: () => void;
  initialValue: IStorage;
  visible: boolean;
  title: string;
}

const StorageModal = (props: IStorageModalProps) => {
  const schema = yup.object().shape({
    city: yup
      .string()
      .required("Місто є обов'язковим полем. ")
      .min(2, "Місто має містити мінімум 2 символи. ")
      .max(50, "Місто може містити максимум 50 символів."),
    street: yup
      .string()
      .min(2, "Вулиця має містити мінімум 2 символи. ")
      .max(50, "Вулиця може містити максимум 50 символів.")
      .required("Вулиця є обов'язковим полем. "),
    number: yup
      .number()
      .moreThan(0, "Номер будівлі має бути більшим нуля.")
      .required("Номер будівлі є обов'язковим полем. "),
  });

  let formik = useFormik({
    initialValues: {
      id: props.initialValue.id ?? 0,
      city: props.initialValue.city,
      street: props.initialValue.street,
      number: props.initialValue.number,
    },
    validationSchema: schema,
    onSubmit: props.onSubmit,
    enableReinitialize: true,
  });

  useEffect(() => {
    formik.setValues(
      {
        id: props.initialValue.id ?? 0,
        city: props.initialValue.city,
        street: props.initialValue.street,
        number: props.initialValue.number,
      },
      false
    );
  }, [props.initialValue]);

  useEffect(() => {
    const fetchStorages = async () => {
      try {
        //let storagesResponse = await storageService.getAllStorages();
        //let workerTypesResponse = await workerTypeService.getWorkerTypes();
        //setStorages(storagesResponse.data);
        //setWorkerTypes(workerTypesResponse.data);
      } catch (error) {
        toast.error(error.response.data);
      }
    };

    //fetchStorages();
  }, []);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group as={Col}>
              <Form.Label>Місто</Form.Label>
              <Form.Control
                type="text"
                name="city"
                max={50}
                min={2}
                placeholder="Введіть місто"
                defaultValue={formik.values.city}
                value={formik.values.city}
                isValid={formik.touched?.city && !formik.errors.city}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.city}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.city}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>Вулиця</Form.Label>
              <Form.Control
                type="text"
                name="street"
                max={50}
                min={2}
                placeholder="Введіть вулицю"
                defaultValue={formik.values.street}
                value={formik.values.street}
                isValid={formik.touched?.street && !formik.errors.street}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.street}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.street}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>Номер будівлі</Form.Label>
              <Form.Control
                type="number"
                name="number"
                placeholder="Введіть номер будівлі"
                defaultValue={formik.values.number}
                value={formik.values.number}
                isValid={formik.touched?.number && !formik.errors.number}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.number}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.number}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default StorageModal;
