import Layout from "./components/Layout";
import NavigationBar from "./components/nav-bar/NavigationBar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MedicinePage from "./medicines/MedicinePage";
import HomePage from "./home/HomePage";
import OrderPage from "./orders/OrderPage";
import SchedulePage from "./schedule/SchedulePage";
import AdminStoragePage from "./storages/AdminStoragePage";
import DecommissionPage from "./decommissions/DecommissionPage";
import "font-awesome/css/font-awesome.min.css";
import LoginPage from "./account/LoginPage";
import ProtectedRoute from "./routes/ProtectedRoute";
import ManagerProtectedRoute from "./routes/ManagerProtectedRoute";
import AdminProtectedRoute from "./routes/AdminProtectedRoute";
import AccountPage from "./account/AccountPage";
import StoragePage from "./storages/StoragePage";
import WorkerPage from "./workers/WorkerPage";
import SupplierPage from "./suppliers/SupplierPage";
import SupplyPage from "./supplies/SupplyPage";
import AdminSchedulePage from "./schedule/AdminSchedulePage";
import Status403 from './status-pages/Status403';
import CustomerPage from "./customers/CustomerPage";

function App() {
  return (
    <>
      <NavigationBar />
      <Layout>
        <Router>
          <Switch>
            <ProtectedRoute
              path="/account"
              component={AccountPage}
            ></ProtectedRoute>
             <ProtectedRoute
              path="/customers"
              component={CustomerPage}
            ></ProtectedRoute>
            <ProtectedRoute path="/home" component={HomePage}></ProtectedRoute>
            <ProtectedRoute
              path="/orders"
              component={OrderPage}
            ></ProtectedRoute>
            <ProtectedRoute
              path="/shifts"
              component={SchedulePage}
            ></ProtectedRoute>
            <ProtectedRoute
              path="/home"
              exact
              component={HomePage}
            ></ProtectedRoute>
            <ManagerProtectedRoute
              path="/decommissions"
              component={DecommissionPage}
            ></ManagerProtectedRoute>
            <ManagerProtectedRoute
              path="/supplies"
              component={SupplyPage}
            ></ManagerProtectedRoute>
            <AdminProtectedRoute
              path="/admin/medicines"
              component={MedicinePage}
            ></AdminProtectedRoute>
            <AdminProtectedRoute
              path="/admin/shifts"
              component={AdminSchedulePage}
            ></AdminProtectedRoute>
            <AdminProtectedRoute
              path="/admin/storages"
              component={AdminStoragePage}
            ></AdminProtectedRoute>
            <AdminProtectedRoute
              path="/admin/workers"
              component={WorkerPage}
            ></AdminProtectedRoute>
            <AdminProtectedRoute
              path="/admin/suppliers"
              component={SupplierPage}
            ></AdminProtectedRoute>
            <Route exact path="/login" component={LoginPage}></Route>
            <Route exact path="/401" component={LoginPage} />
            <Route exact path="/403" component={Status403} />
            <ManagerProtectedRoute
              path="/storages/:id"
              component={StoragePage}
            ></ManagerProtectedRoute>
            <ManagerProtectedRoute
              exact
              path="/storages"
              component={StoragePage}
            ></ManagerProtectedRoute>
          </Switch>
        </Router>
      </Layout>
    </>
  );
}
//
export default App;
