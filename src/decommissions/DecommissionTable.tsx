import { BiEditAlt } from "react-icons/bi";
import Table from "../components/Table";
import React, { useState } from "react";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import IDecommission from "./data/IDecommission";
import moment from "moment";
import DecommissionDetailsModal from "./DecommissionDetailsModal";

interface IDecommissionTableProps {
  data: IDecommission[];
  loading: boolean;
}

const DecommissionTable = (props: IDecommissionTableProps) => {
  const { data } = props;
  const [visible, setVisible] = useState(false);
  const [activeDecommission, setActiveDecommission] = useState<IDecommission>(
    {} as IDecommission
  );
  const handleClose = () => {
    setVisible(false);
  };

  const columns = [
    {
      name: "Номер списання",
      selector: "id",
      sortable: true,
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      cell: (row: any) => <>{moment(row.date).format("MMM DD, YYYY")}</>,
    },
    {
      name: "Сума",
      selector: "price",
      sortable: true,
      cell: (row: any) => <>{row.price} грн</>,
    },
    {
      name: "Склад",
      selector: "storage",
      sortable: false,
      cell: (row: any) => (
        <>
          №{row.storage?.id} - м. {row.storage?.city}, вул.{" "}
          {row.storage?.street} буд. {row.storage?.number}
        </>
      ),
    },
  ];

  return (
    <>
      <Table
        columns={columns}
        data={data}
        loading={props.loading}
        title={"Списання товарів"}
        onRowClicked={(row: any) => {
          setActiveDecommission(row);
          setVisible(true);
        }}
      />
      <DecommissionDetailsModal
        visible={visible}
        handleClose={handleClose}
        data={activeDecommission}
      />
      <Toast />
    </>
  );
};

export default DecommissionTable;
