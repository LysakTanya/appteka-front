import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import DecommissionTable from "./DecommissionTable";
import IDecommission from "./data/IDecommission";
import decommissionService from "./data/decommissionService";
import { toast } from "react-toastify";
import { Button, Tab, Tabs } from "react-bootstrap";
import { AiFillMedicineBox } from "react-icons/ai";
import styles from "../orders/styles/orders.module.css";
import CreateDecommission from "./CreateDecommission";

const DecommissionPage = () => {
  const [decommissions, setDecommissions] = useState<IDecommission[]>(
    [] as IDecommission[]
  );
  const [loading, setLoading] = useState(false);
  const [createMode, setCreateMode] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await decommissionService.getDecommissions();
        setDecommissions(response.data);
        console.log({ dec: response.data });
      } catch (error) {
        toast.error(error.response?.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  const FirstStep = () => {
    return (
      <div className={styles.firstStepContainer}>
        <h3>
          Створити нове списання <AiFillMedicineBox size="4rem" />
        </h3>
        <Button
          onClick={() => {
            setCreateMode(true);
          }}
        >
          Перейти до створення
        </Button>
      </div>
    );
  };

  return (
    <div>
      <Tabs defaultActiveKey="newDecommission">
        <Tab title="Нове списання" eventKey="newDecommission">
          {createMode ? (
            <CreateDecommission handleLeave={() => setCreateMode(false)} />
          ) : (
            <FirstStep />
          )}
        </Tab>
        <Tab title="Списання" eventKey="decommissions">
          <DecommissionTable loading={loading} data={decommissions} />
        </Tab>
      </Tabs>
    </div>
  );
};

export default DecommissionPage;
