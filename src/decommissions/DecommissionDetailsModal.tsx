import moment from "moment";
import Table from "../components/Table";
import { BiCheckDouble, BiX, BiTrash } from "react-icons/bi";
import { toast } from "react-toastify";
import { Button, Modal } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import React, { useRef } from "react";
import { useReactToPrint } from "react-to-print";
import styles from "./styles/supply.module.css";
import DataTable from "react-data-table-component";
import IDecommission from "./data/IDecommission";

interface IDecommissionDetailsModalProps {
  data: IDecommission;
  visible: boolean;
  handleClose: () => void;
}

const DecommissionDetailsModal = (props: IDecommissionDetailsModalProps) => {
  const { data } = props;
  const componentToPrintRef = useRef<any>();
  const handlePrint = useReactToPrint({
    content: () => componentToPrintRef.current,
  });

  const columns = [
    {
      name: "Код товару",
      selector: "medicine",
      sortable: true,
      cell: (row: any) => <div>{row.medicine?.code}</div>
    },
    {
      name: "Назва",
      selector: "medicine",
      sortable: true,
      cell: (row: any) => <div>{row.medicine?.name}</div>
    },
    {
      name: "Категорія",
      selector: "medicine",
      sortable: true,
      cell: (row: any) => <div>{row.medicine?.category?.name}</div>
    },
    {
      name: "Кількість",
      selector: "count",
      sortable: false,
    },
    {
      name: "Ціна",
      selector: "price",
      sortable: false,
      cell: (row: any) => <div>{row.price} грн</div>,
    },
    {
      name: "Сума",
      selector: "price",
      sortable: false,
      cell: (row: any) => <div>{row.price * row.count} грн</div>,
    },
  ];

  return (
    <>
      <div style={{ display: "none" }}>
        <div ref={componentToPrintRef} style={{ margin: 10 }}>
          <Modal.Title>Списання №{data.id}</Modal.Title>
          <p>
            <b>Дата:</b> {moment(data.date).format("MM/DD/YYYY HH:mm a")}
          </p>
          <p>
            <b>Торгова точка: </b> №{data.storage?.id} - м. {data.storage?.city}
            , вул. {data.storage?.street} буд. {data.storage?.number}
          </p>
          <hr />
          <DataTable
            title={"Товари"}
            columns={columns}
            data={data.medicineDecommissions}
          />
          <hr />
          <h4 style={{ margin: 20 }}>Загальна сума: {data.price} грн</h4>
        </div>
      </div>

      <Modal
        size="xl"
        dialogClassName="large-modal"
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>Списання №{data.id}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <p>
            <b>Дата:</b> {moment(data.date).format("MM/DD/YYYY HH:mm a")}
          </p>
          <p>
            <b>Торгова точка: </b> №{data.storage?.id} - м. {data.storage?.city}
            , вул. {data.storage?.street} буд. {data.storage?.number}
          </p>
          <Table
            title={"Товари"}
            columns={columns}
            loading={false}
            data={data.medicineDecommissions}
          />
          <h4 style={{ margin: 20 }}>Загальна сума: {data.price} грн</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={props.handleClose}>
            Закрити
          </Button>
          <Button variant="info" onClick={handlePrint}>
            Роздрукувати
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default DecommissionDetailsModal;
