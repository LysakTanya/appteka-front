import ICreateMedicineDecommissionModel from "./ICreateMedicineDecommission";

interface ICreateDecommissionModel {
    medicineDecommissions: ICreateMedicineDecommissionModel[];
}

export default ICreateDecommissionModel;