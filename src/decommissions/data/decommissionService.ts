import IDecommission from "./IDecommission";
import Routes from "../../Routes";
import Axios from "../../Axios";
import ICreateDecommissionModel from "./ICreateDecommissionModel";

const decommissionService = {
  getDecommissions: async () =>
    Axios.get<IDecommission[]>(Routes.decommissionRoute),

  addDecommission: async (decommission: ICreateDecommissionModel) =>
    Axios.post<IDecommission>(Routes.decommissionRoute, decommission),
};

export default decommissionService;
