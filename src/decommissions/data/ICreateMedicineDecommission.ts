interface ICreateMedicineDecommissionModel {
    medicineId: number;
    count: number;
    price: number;
}

export default ICreateMedicineDecommissionModel;