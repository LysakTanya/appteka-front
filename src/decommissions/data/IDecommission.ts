import IStorage from "../../storages/data/IStorage";
import IMedicineDecommission from "./IMedicineDecommission";

interface IDecommission {
    id: number;
    date: Date;
    price: number;
    medicineDecommissions: IMedicineDecommission[];
    storage: IStorage;
}

export default IDecommission;