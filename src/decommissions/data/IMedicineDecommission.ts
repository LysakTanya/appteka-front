import IMedicine from "../../medicines/data/IMedicine";

interface IMedicineDecommission {
    price: number;
    count: number;
    medicine: IMedicine;
}

export default IMedicineDecommission;