import { FormikErrors, useFormik } from "formik";
import { Modal, Button, Form, Alert } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import * as yup from "yup";
import IStorageMedicine from "../storages/data/IStorageMedicine";
import IMedicineDecommission from "./data/IMedicineDecommission";

interface IMedicineDecommissionModalProps {
  visible: boolean;
  handleClose: () => void;
  title: string;
  storageMedicine: IStorageMedicine;
  handleAddMedicine: (medicineDecommission: IMedicineDecommission) => void;
}

const MedicineDecommissionModal = (props: IMedicineDecommissionModalProps) => {
  const { storageMedicine } = props;
  const schema = yup.object().shape({
    count: yup
      .number()
      .required("Кількість є обов'язковим полем. ")
      .moreThan(0, "Кількість має бути більшою за 0.")
      .max(
        storageMedicine?.count,
        "Кількість не може бути більша за наявну на складі."
      ),
    price: yup.number().required().moreThan(0),
  });

  let formik = useFormik({
    initialValues: {
      count: 1,
      price: 1,
    },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      const medicineDecommission: IMedicineDecommission = {
        medicine: storageMedicine.medicine,
        count: values.count,
        price: values.price,
      };

      props.handleAddMedicine(medicineDecommission);
      props.handleClose();
    },
  });

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Alert variant="success">
            <Alert.Heading>{storageMedicine.medicine?.code}</Alert.Heading>
            <p>
              {storageMedicine.medicine?.name} -{" "}
              {storageMedicine.medicine?.category?.name}
            </p>
            <hr />
            <p className="mb-0">
              {storageMedicine.medicine?.quantity}{" "}
              {storageMedicine.medicine?.category?.measure}
            </p>
            <p>
              <b>Ціна: </b>
              {storageMedicine.medicine?.price} грн
            </p>
          </Alert>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Кількість/од</Form.Label>
              <Form.Control
                type="number"
                name="count"
                placeholder="Введіть кількість"
                defaultValue={formik.values.count}
                isValid={formik.touched?.count && !formik.errors.count}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.count}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.count}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Ціна/од</Form.Label>
              <Form.Control
                type="number"
                name="price"
                placeholder="Введіть ціну"
                defaultValue={formik.values.price}
                isValid={formik.touched?.price && !formik.errors.price}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.price}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.price}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default MedicineDecommissionModal;
