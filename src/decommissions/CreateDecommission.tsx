import React, { useEffect, useState } from "react";
import { Container, Button, Form, Navbar } from "react-bootstrap";
import { toast } from "react-toastify";
import { confirmAlert } from "react-confirm-alert";
import storageService from "../storages/data/storageService";
import IStorageMedicine from "../storages/data/IStorageMedicine";
import styles from "../orders/styles/orders.module.css";
import Toast from "../components/Toast";
import IMedicineDecommission from "./data/IMedicineDecommission";
import ICreateDecommissionModel from "./data/ICreateDecommissionModel";
import MedicineTable from "../orders/MedicineTable";
import MedicineDecommissionTable from "./MedicineDecommissionTable";
import decommissionService from "./data/decommissionService";
import DecommissionDetailsModal from "./DecommissionDetailsModal";
import IDecommission from "./data/IDecommission";
import StorageMedicineTable from "./StorageMedicineTable";

const CreateDecommission = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [medicineDecommissions, setMedicineDecommissions] = useState<
    IMedicineDecommission[]
  >([] as IMedicineDecommission[]);
  const storageId = Number(localStorage.getItem("storageId"));
  const [storageMedicines, setStorageMedicines] = useState<IStorageMedicine[]>(
    [] as IStorageMedicine[]
  );
  const [filteredMedicines, setFilteredMedicines] = useState<
    IStorageMedicine[]
  >([] as IStorageMedicine[]);
  const [visible, setVisible] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const handleSearch = () => {
    if (searchValue === "") {
      setFilteredMedicines(storageMedicines);
      return;
    }

    const filteredMedicines = storageMedicines.filter(
      (item) =>
        item.medicine?.name.includes(searchValue) ||
        item.medicine?.code.includes(searchValue)
    );
    setFilteredMedicines(filteredMedicines);
  };

  const [decommission, setDecommission] = useState<IDecommission>(
    {} as IDecommission
  );

  const handleAddMedicineToDecommission = (
    inputMedicineDecommission: IMedicineDecommission
  ) => {
    setStorageMedicines((prev) => {
      const oldMedicine = prev.find(
        (item) => item.medicine.id == inputMedicineDecommission.medicine.id
      );
      const filtered = prev.filter(
        (item) => item.medicine.id !== inputMedicineDecommission.medicine.id
      );
      const newMedicine: IStorageMedicine = {
        medicine: oldMedicine!.medicine,
        count: oldMedicine!.count - inputMedicineDecommission.count,
      };
      return filtered.concat(newMedicine);
    });
    setMedicineDecommissions((prev) => {
      const index = prev.findIndex(
        (item) => item.medicine?.id === inputMedicineDecommission.medicine?.id
      );
      if (index !== -1) {
        const medicineOrder = prev[index];
        const newMedicineOrder = {
          medicine: medicineOrder.medicine,
          price: inputMedicineDecommission.price,
          count: medicineOrder.count + inputMedicineDecommission.count,
        };
        const filtered = prev.filter((item, i) => i !== index);
        return filtered.concat(newMedicineOrder);
      }

      return prev.concat(inputMedicineDecommission);
    });
  };

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        const response = await storageService.getStorageMedicines();
        setStorageMedicines(response.data);
        setFilteredMedicines(response.data);
      } catch (error) {
        toast.error(error.response.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    handleSearch();
  }, [storageMedicines]);

  const handleRemoveMedicineOrder = (
    medicineDecommission: IMedicineDecommission
  ) => {
    setStorageMedicines((prev) => {
      const oldMedicine = prev.find(
        (item) => item.medicine.id === medicineDecommission.medicine.id
      );
      const filtered = prev.filter(
        (item) => item.medicine.id !== medicineDecommission.medicine.id
      );
      const newMedicine: IStorageMedicine = {
        medicine: oldMedicine!.medicine,
        count: oldMedicine!.count + medicineDecommission.count,
      };
      return filtered.concat(newMedicine);
    });
    setMedicineDecommissions((prev) => {
      return prev.filter(
        (item) => item.medicine?.id !== medicineDecommission.medicine.id
      );
    });
  };

  const handleAddDecommission = (e: any) => {
    if (
      medicineDecommissions == undefined ||
      medicineDecommissions.length === 0
    ) {
      toast.error(
        "Списання не може містити 0 товарів. Додайте хоча б один товар до списання."
      );
      return;
    }

    confirmAlert({
      message: "Ви впевнені, що хочете створити списання?",
      title: "Нове списання",
      buttons: [
        {
          label: "Так",
          onClick: () => {
            const decommission: ICreateDecommissionModel = {
              medicineDecommissions: medicineDecommissions.map((item) => {
                return {
                  medicineId: item.medicine.id,
                  count: item.count,
                  price: item.price,
                };
              }),
            };

            decommissionService
              .addDecommission(decommission)
              .then((response) => {
                setDecommission(response.data);
                setVisible(true);
              })
              .catch((error) => toast.error(error.response?.data));
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const clearValues = () => {
    setSearchValue("");
    setFilteredMedicines(storageMedicines);
    setMedicineDecommissions([]);
  };

  const handleReset = () => {
    confirmAlert({
      title: "Відміна",
      message: "Всі зміни будуть скинуті.",
      buttons: [
        {
          label: "Ок",
          onClick: () => {
            clearValues();
            props.handleLeave();
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const handleClose = () => {
    setVisible(false);
    window.location.reload();
  };

  return (
    <>
      <div className={styles.newOrderContainer}>
        <div className={styles.newOrderSubcontainer}>
          <Navbar
            bg="primary"
            variant="dark"
            style={{ marginTop: 20, marginBottom: 5 }}
          >
            <Form className={styles.navForm}>
              <Form.Label
                style={{
                  flex: 1.5,
                  alignSelf: "center",
                  marginLeft: 5,
                  color: "white",
                  fontWeight: 700,
                }}
              >
                Пошук товарів
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                value={searchValue}
                style={{ margin: 5, flex: 4 }}
                onChange={(e: any) => setSearchValue(e.target.value)}
              />
              <Button
                variant="outline-light"
                style={{ margin: 5, flex: 1 }}
                onClick={handleSearch}
              >
                Пошук
              </Button>
              <Button
                variant="outline-warning"
                style={{ margin: 5, flex: 1 }}
                type="reset"
                onClick={() => {
                  setSearchValue("");
                  setFilteredMedicines(storageMedicines);
                }}
              >
                Очистити
              </Button>
            </Form>
          </Navbar>
          <StorageMedicineTable
            data={filteredMedicines}
            loading={loading}
            handleAddMedicine={handleAddMedicineToDecommission}
          />
        </div>
        <div style={{ width: 2, backgroundColor: "black", margin: 15 }} />
        <div className={styles.newOrderSubcontainer}>
          <Container>
            <h3>Оформлення списання</h3>
          </Container>
          <hr />
          <MedicineDecommissionTable
            medicineDecommissions={medicineDecommissions}
            handleRemoveMedicine={handleRemoveMedicineOrder}
          />
          <hr />
          <p>
            <b>Загальна сума: </b>
            {medicineDecommissions?.reduce(
              (total, current) =>
                total + current.count * current.price,
              0
            ) ?? 0}{" "}
            грн
          </p>
          <div className={styles.buttonContainer}>
            <Button
              variant="primary"
              onClick={handleAddDecommission}
              style={{ margin: 10 }}
            >
              Створити
            </Button>
            <Button
              variant="warning"
              onClick={handleReset}
              style={{ margin: 10 }}
            >
              Відмінити
            </Button>
          </div>
        </div>
      </div>
      <Toast />
      <DecommissionDetailsModal
        data={decommission}
        visible={visible}
        handleClose={handleClose}
      />
    </>
  );
};

export default CreateDecommission;
