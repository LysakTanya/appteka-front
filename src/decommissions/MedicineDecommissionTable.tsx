import React from "react";
import { BiTrash } from "react-icons/bi";
import DataTable from "react-data-table-component";
import IMedicineDecommission from "./data/IMedicineDecommission";

interface IMedicineDecommissionTableProps {
  medicineDecommissions: IMedicineDecommission[];
  handleRemoveMedicine: (medicineDecommission: IMedicineDecommission) => void;
}

const MedicineDecommissionTable = (props: IMedicineDecommissionTableProps) => {
  const columns = [
    {
      name: "Код",
      selector: "medicine",
      sortable: false,
      compact: true,
      width: "17%",
      cell: (row: any) => <div>{row.medicine?.code}</div>,
    },
    {
      name: "Назва",
      selector: "medicine",
      sortable: false,
      compact: true,
      width: "35%",
      cell: (row: any) => <div>{row.medicine?.name}</div>,
    },
    {
      name: "Кількість",
      selector: "count",
      sortable: true,
      width: "10%",
      compact: true,
    },
    {
      compact: true,
      name: "Ціна",
      selector: "price",
      sortable: true,
      width: "13%",
    },
    {
      name: "Повна сума",
      selector: "price",
      compact: true,
      sortable: false,
      width: "15%",
      cell: (row: any) => {
        return <div>{row.price * row.count} грн</div>;
      },
    },
    {
      name: "",
      selector: "price",
      sortable: false,
      right: true,
      width: "10%",
      cell: (row: any) => {
        return (
          <BiTrash
            size="1.7rem"
            onClick={() => {
              props.handleRemoveMedicine(row);
            }}
          />
        );
      },
    },
  ];

  return (
    <>
      <DataTable
        columns={columns}
        data={props.medicineDecommissions}
        title="Товари"
        dense={true}
        pagination={true}
        noDataComponent={<div>Ще немає товарів у кошику списання. </div>}
      />
    </>
  );
};

export default MedicineDecommissionTable;
