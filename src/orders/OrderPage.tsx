import React, { useState, useEffect } from "react";
import medicineService from "../medicines/data/medicineService";
import Medicine from "../medicines/data/IMedicine";
import { Button, Tab, Tabs } from "react-bootstrap";
import CreateOrder from "./CreateOrder";
import styles from "./styles/orders.module.css";
import { AiFillMedicineBox } from "react-icons/ai";
import OrderTable from "./OrderTable";
import IOrder from "./data/IOrder";
import { toast } from "react-toastify";
import orderService from "./data/orderService";
import Toast from "../components/Toast";

const OrderPage = () => {
  const [createMode, setCreateMode] = useState(false);
  const [orders, setOrders] = useState<IOrder[]>([] as IOrder[]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await orderService.getOrders();
        setOrders(response.data);
      } catch (error) {
        toast.error(error.response?.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  const FirstStep = () => {
    return (
      <div className={styles.firstStepContainer}>
        <h3>
          Створити нове замовлення <AiFillMedicineBox size="4rem" />
        </h3>
        <Button
          onClick={() => {
            setCreateMode(true);
          }}
        >
          Перейти до створення
        </Button>
      </div>
    );
  };

  return (
    <div>
      <Tabs defaultActiveKey="newOrder">
        <Tab title="Нове замовлення" eventKey="newOrder">
          {createMode ? (
            <CreateOrder handleLeave={() => setCreateMode(false)} />
          ) : (
            <FirstStep />
          )}
        </Tab>
        <Tab title="Замовлення" eventKey="orders">
          <OrderTable data={orders} loading={loading} />
        </Tab>
      </Tabs>
      <Toast />
    </div>
  );
};

export default OrderPage;
