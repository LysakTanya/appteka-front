import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import { toast } from "react-toastify";
import ICreateOrderModel from "./data/ICreateOrderModel";
import IOrder from "./data/IOrder";
import orderService from "./data/orderService";
import OrderDetailsModal from "./OrderDetailsModal";

interface IConfirmationOrderModalProps {
  order: ICreateOrderModel;
  visible: boolean;
  handleClose: () => void;
}

const ConfirmationOrder = (props: IConfirmationOrderModalProps) => {
  const calculateFullPrice = () => {
    var sum =
      props.order?.medicineOrders?.reduce(
        (total, current) => total + current.count * current.price,
        0
      ) ?? 0;
    if (props.order.customerId) {
      sum -= sum * 0.05;
    }

    return sum;
  };

  const [inputMoney, setInputMoney] = useState(0);
  const [visible, setVisible] = useState(false);

  const handlePayment = () => {
    if (inputMoney < calculateFullPrice()) {
      toast.success("Внесіть суму рівну або більшу зазначеної.");
      return;
    }

    setIsPaid(true);
  };

  const handleComplete = () => {
    orderService
      .addOrder(props.order)
      .then((response) => {
        toast.success("Замовлення успішно виконане.");
        props.handleClose();
        setVisible(true);
        setOrder(response.data);
      })
      .catch((error) =>
        toast.error("При здійсненні замовлення виникла помилка.")
      );
  };

  const handleClose = () => {
    setVisible(false);
    window.location.reload();
  };
  const [order, setOrder] = useState<IOrder>({} as IOrder);

  const [isPaid, setIsPaid] = useState(false);
  return (
    <>
      <Modal
        show={props.visible}
        size="lg"
        onHide={() => {
          props.handleClose();
        }}
        backdrop="static"
      >
        <Modal.Header>
          <Modal.Title>Замовлення</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <h4>До сплати: {calculateFullPrice()} грн</h4>
          <Form.Group>
            {!isPaid ? (
              <div>
                <Form.Label>Внести суму (в грн)</Form.Label>
                <Form.Control
                  type="number"
                  value={inputMoney}
                  onChange={(e: any) => setInputMoney(e.target.value)}
                />
                <Button
                  variant="primary"
                  onClick={handlePayment}
                  style={{ margin: 10 }}
                >
                  Оплата
                </Button>
              </div>
            ) : (
              <div>
                <h4>Внесено: {inputMoney} грн</h4>
                <h4>Решта: {(inputMoney - calculateFullPrice()).toPrecision(2)} грн</h4>
                <Button
                  variant="success"
                  onClick={handleComplete}
                  style={{ margin: 10 }}
                >
                  Завершити
                </Button>
              </div>
            )}
          </Form.Group>
        </Modal.Body>
      </Modal>
      <OrderDetailsModal visible={visible} handleClose={handleClose} data={order}/>
    </>
  );
};

export default ConfirmationOrder;
