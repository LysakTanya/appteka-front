import { BiEditAlt } from "react-icons/bi";
import Table from "../components/Table";
import { useState } from "react";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import IOrder from "./data/IOrder";
import OrderDetailsModal from "./OrderDetailsModal";
import moment from "moment";

interface IOrderTableProps {
  data: IOrder[];
  loading: boolean;
}

const OrderTable = (props: IOrderTableProps) => {
  const { data } = props;
  const [visible, setVisible] = useState(false);
  const [activeOrder, setActiveOrder] = useState<IOrder>({} as IOrder);
  const handleClose = () => {
    setVisible(false);
  };

  const columns = [
    {
      name: "№",
      selector: "id",
      sortable: true,
      width: "5%",
    },
    {
      name: "Склад",
      selector: "storage",
      sortable: true,
      cell: (row: any) => (
        <>
          №{row.storage?.id} - м. {row.storage?.city}, вул.{" "}
          {row.storage?.street} буд. {row.storage?.number}
        </>
      ),
    },
    {
      name: "Дата",
      selector: "date",
      sortable: true,
      cell: (row: any) => (
        <div>{moment(row.date).format("MM/DD/YYYY HH:mm a")}</div>
      ),
    },
    {
      name: "Фармацевт",
      selector: "pharmacist",
      sortable: true,
      cell: (row: any) => (
        <>
          {row.pharmacist?.name} {row.pharmacist?.surname}
        </>
      ),
    },
    {
      name: "Клієнт",
      selector: "customer",
      sortable: false,
      cell: (row: any) => (
        <>
          {row.customer ? (
            <div>
              {row.customer?.name} {row.customer?.surname}
            </div>
          ) : (
            <div>-</div>
          )}
        </>
      ),
    },
    {
      name: "Повна сума",
      selector: "price",
      sortable: true,
      cell: (row: any) => <div>{row.price} грн</div>,
    },
  ];

  return (
    <>
      <Table
        columns={columns}
        data={data}
        loading={props.loading}
        title={"Торгові точки"}
        onRowClicked={(row: any) => {
          setActiveOrder(row);
          setVisible(true);
        }}
      />
      <OrderDetailsModal
        handleClose={handleClose}
        visible={visible}
        data={activeOrder}
      />
      <Toast />
    </>
  );
};

export default OrderTable;
