import ICustomer from "../../customers/data/ICustomer";
import IStorage from "../../storages/data/IStorage";
import IWorker from "../../workers/data/IWorker";
import IMedicineOrder from "./IMedicineOrder";

interface IOrder {
    id: number;
    customer: ICustomer | null;
    price: number;
    pharmacist: IWorker;
    date: Date;
    medicineOrders: IMedicineOrder[];
    storage: IStorage;
}

export default IOrder;