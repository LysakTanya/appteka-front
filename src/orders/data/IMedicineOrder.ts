import IMedicine from "../../medicines/data/IMedicine";

interface IMedicineOrder {
    count: number;
    price: number;
    medicine: IMedicine;
};

export default IMedicineOrder;