import IOrder from "./IOrder";
import Routes from "../../Routes";
import Axios from "../../Axios";
import ICreateOrderModel from "./ICreateOrderModel";

const orderService = { 
  getOrders: async () => Axios.get<IOrder[]>(Routes.orderRoute),

  addOrder: async (order: ICreateOrderModel) => Axios.post<IOrder>(Routes.orderRoute, order),
  
};

export default orderService;
