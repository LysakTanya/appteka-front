import IMedicine from "../../medicines/data/IMedicine";

interface ICreateMedicineOrderModel {
    count: number;
    price: number;
    medicineId: number;
};

export default ICreateMedicineOrderModel;