import ICreateMedicineOrderModel from "./ICreateMedicineOrderModel";

interface ICreateOrderModel {
    customerId: number | null;
    medicineOrders: ICreateMedicineOrderModel[];
}

export default ICreateOrderModel;