import moment from "moment";
import Table from "../components/Table";
import { BiCheckDouble, BiX, BiTrash } from "react-icons/bi";
import { toast } from "react-toastify";
import { Button, Modal } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import React, { useRef } from "react";
import { useReactToPrint } from "react-to-print";
import styles from "./styles/supply.module.css";
import DataTable from "react-data-table-component";
import IOrder from "./data/IOrder";

interface IOrderDetailsModalProps {
  data: IOrder;
  visible: boolean;
  handleClose: () => void;
}

const OrderDetailsModal = (props: IOrderDetailsModalProps) => {
  const { data } = props;
  const componentToPrintRef = useRef<any>();
  const handlePrint = useReactToPrint({
    content: () => componentToPrintRef.current,
  });

  const columns = [
    {
      name: "Назва товару",
      selector: "medicine.name",
      sortable: true,
    },
    {
      name: "Кількість/од",
      selector: "count",
      sortable: true,
    },
    {
      name: "Ціна",
      selector: "price",
      sortable: true,
      cell: (row: any) => <div>{row.price} грн</div>,
    },
    {
      name: "Сума",
      selector: "price",
      sortable: false,
      cell: (row: any) => <div>{row.price * row.count} грн</div>,
    },
  ];

  return (
    <>
      <div style={{ display: "none" }}>
        <div ref={componentToPrintRef} style={{ margin: 10 }}>
        <Modal.Title>Чек №{data.id}</Modal.Title>
          <p>
            <b>Дата:</b> {moment(data.date).format("MM/DD/YYYY HH:mm a")}
          </p>
          {data.customer && (
            <p>
              <b>Покупець:</b> {data.customer?.name} {data.customer?.surname}
            </p>
          )}
          <p>
            <b>Місце покупки: </b> №{data.storage?.id} - м. {data.storage?.city}
            , вул. {data.storage?.street} буд. {data.storage?.number}
          </p>
          <p>
            <b>Фармацевт: </b> {data.pharmacist?.name} {data.pharmacist?.surname}
          </p>
          <hr/>
          <DataTable
            title={"Товари"}
            columns={columns}
            data={data.medicineOrders}
          />
          <hr/>
          <h4 style={{ margin: 20 }}>Загальна сума: {data.price} грн</h4>
        </div>
      </div>

      <Modal
        size="xl"
        dialogClassName="large-modal"
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>Чек №{data.id}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <p>
            <b>Дата:</b> {moment(data.date).format("MM/DD/YYYY HH:mm a")}
          </p>
          {data.customer && (
            <p>
              <b>Покупець:</b> {data.customer?.name} {data.customer?.surname}
            </p>
          )}
          <p>
            <b>Місце покупки: </b> №{data.storage?.id} - м. {data.storage?.city}
            , вул. {data.storage?.street} буд. {data.storage?.number}
          </p>
          <p>
            <b>Фармацевт: </b> {data.pharmacist?.name} {data.pharmacist?.surname}
          </p>
          <Table
            title={"Товари"}
            columns={columns}
            loading={false}
            data={data.medicineOrders}
          />
           <h4 style={{ margin: 20 }}>Загальна сума: {data.price} грн</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={props.handleClose}>
            Закрити
          </Button>
          <Button variant="info" onClick={handlePrint}>
            Роздрукувати
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default OrderDetailsModal;
