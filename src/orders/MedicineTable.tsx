import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BiAddToQueue } from "react-icons/bi";
import DataTable from "react-data-table-component";
import IMedicineOrder from "./data/IMedicineOrder";
import IStorageMedicine from "../storages/data/IStorageMedicine";
import MedicineOrderModal from "./MedicineOrderModal";
import MedicineModal from "../medicines/MedicineModal";

interface IMedicineTableProps {
  data: IStorageMedicine[];
  loading: boolean;
  handleAddMedicine: (medicineOrder: IMedicineOrder) => void;
}

const MedicineTable = (props: IMedicineTableProps) => {
  const [visible, setVisible] = useState(false);
  const handleClose = () => {
    setVisible(false);
  };
  const [
    activeStorageMedicine,
    setActiveStorageMedicine,
  ] = useState<IStorageMedicine>({} as IStorageMedicine);

  const [detailsVisible, setDetailsVisible] = useState(false);
  const columns = [
    {
      name: "Код",
      selector: "medicine.code",
      sortable: false,
      left: true,
      compact: true,
      width: "20%",
    },
    {
      name: "Назва",
      selector: "medicine.name",
      sortable: true,
      left: true,
      compact: true,
      width: "35%",
    },
    {
      name: "К-сть",
      selector: "medicine.quantity",
      sortable: true,
      left: true,
      compact: true,
      width: "7%",
    },
    {
      name: "Міра",
      selector: "medicine.category",
      sortable: false,
      compact: true,
      left: true,
      width: "7%",
      cell: (row: any) => {
        return <div>{row.medicine?.category?.measure}</div>;
      },
    },
    {
      name: "Ціна",
      selector: "medicine.price",
      sortable: true,
      compact: true,
      left: true,
      width: "10%",
    },
    {
      name: "Наявно",
      selector: "count",
      sortable: true,
      compact: true,
      left: true,
      width: "10%",
    },
    {
      name: "",
      selector: "count",
      sortable: false,
      compact: true,
      right: true,
      width: "5%",
      cell: (row: any) => {
        if (row.count === 0) {
          return <></>;
        }

        return (
          <BiAddToQueue
            size="1.7rem"
            onClick={() => {
              setActiveStorageMedicine(row);
              setVisible(true);
            }}
          />
        );
      },
    },
  ];

  return (
    <>
      <MedicineOrderModal
        visible={visible}
        storageMedicine={activeStorageMedicine}
        title="Додати до замовлення"
        handleClose={handleClose}
        handleAddMedicine={props.handleAddMedicine}
      />
      <DataTable
        conditionalRowStyles={[
          {
            when: (row) => row.count === 0,
            style: {
              backgroundColor: "grey",
              color: "white",
              "&:hover": {
                cursor: "cross",
              },
            },
          },
        ]}
        columns={columns}
        data={props.data}
        noHeader
        progressPending={props.loading}
        dense={true}
        pagination={true}
        onRowClicked={(row: any) => {
          setActiveStorageMedicine(row);
          setDetailsVisible(true);
        }}
      />
      <MedicineModal
        data={activeStorageMedicine?.medicine ?? {}}
        visible={detailsVisible}
        handleClose={() => {
          setDetailsVisible(false);
        }}
      />
    </>
  );
};

export default MedicineTable;
