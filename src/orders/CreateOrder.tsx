import React, { useEffect, useState } from "react";
import { Container, Button, Form, Navbar } from "react-bootstrap";
import { toast } from "react-toastify";
import { confirmAlert } from "react-confirm-alert";
import MedicineTable from "./MedicineTable";
import IMedicineOrder from "./data/IMedicineOrder";
import storageService from "../storages/data/storageService";
import IStorageMedicine from "../storages/data/IStorageMedicine";
import MedicineOrderTable from "./MedicineOrderTable";
import ICustomer from "../customers/data/ICustomer";
import styles from "./styles/orders.module.css";
import customerService from "../customers/data/customerService";
import Toast from "../components/Toast";
import { BsX } from "react-icons/bs";
import ICreateOrderModel from "./data/ICreateOrderModel";
import orderService from "./data/orderService";
import ConfirmationOrder from "./ConfirmationOrder";

const CreateOrder = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [medicineOrders, setMedicineOrders] = useState<IMedicineOrder[]>(
    [] as IMedicineOrder[]
  );
  const storageId = Number(localStorage.getItem("storageId"));
  const [storageMedicines, setStorageMedicines] = useState<IStorageMedicine[]>(
    [] as IStorageMedicine[]
  );
  const [filteredMedicines, setFilteredMedicines] = useState<
    IStorageMedicine[]
  >([] as IStorageMedicine[]);
  const [visible, setVisible] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const [customer, setCustomer] = useState<ICustomer>();
  const [customerData, setCustomerData] = useState("");
  const handleSearch = () => {
    if (searchValue === "") {
      setFilteredMedicines(storageMedicines);
      return;
    }

    const filteredMedicines = storageMedicines.filter(
      (item) =>
        item.medicine?.name.includes(searchValue) ||
        item.medicine?.code.includes(searchValue)
    );
    setFilteredMedicines(filteredMedicines);
  };

  const [order, setOrder] = useState<ICreateOrderModel>({} as ICreateOrderModel);

  const handleAddMedicineToOrder = (inputMedicineOrder: IMedicineOrder) => {
    setStorageMedicines((prev) => {
      const oldMedicine = prev.find(
        (item) => item.medicine.id == inputMedicineOrder.medicine.id
      );
      const filtered = prev.filter(
        (item) => item.medicine.id !== inputMedicineOrder.medicine.id
      );
      const newMedicine: IStorageMedicine = {
        medicine: oldMedicine!.medicine,
        count: oldMedicine!.count - inputMedicineOrder.count,
      };
      return filtered.concat(newMedicine);
    });
    setMedicineOrders((prev) => {
      const index = prev.findIndex(
        (item) => item.medicine?.id === inputMedicineOrder.medicine?.id
      );
      if (index !== -1) {
        const medicineOrder = prev[index];
        const newMedicineOrder = {
          medicine: medicineOrder.medicine,
          price: inputMedicineOrder.price,
          count: medicineOrder.count + inputMedicineOrder.count,
        };
        const filtered = prev.filter((item, i) => i !== index);
        return filtered.concat(newMedicineOrder);
      }

      return prev.concat(inputMedicineOrder);
    });
  };

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        const response = await storageService.getStorageMedicines();
        setStorageMedicines(response.data);
        setFilteredMedicines(response.data);
      } catch (error) {
        toast.error(error.response.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    handleSearch();
  }, [storageMedicines]);

  const handleRemoveMedicineOrder = (medicineOrder: IMedicineOrder) => {
    setStorageMedicines((prev) => {
      const oldMedicine = prev.find(
        (item) => item.medicine.id === medicineOrder.medicine.id
      );
      const filtered = prev.filter(
        (item) => item.medicine.id !== medicineOrder.medicine.id
      );
      const newMedicine: IStorageMedicine = {
        medicine: oldMedicine!.medicine,
        count: oldMedicine!.count + medicineOrder.count,
      };
      return filtered.concat(newMedicine);
    });
    setMedicineOrders((prev) => {
      return prev.filter(
        (item) => item.medicine?.id !== medicineOrder.medicine.id
      );
    });
  };

  const handleAddOrder = (e: any) => {
    if (medicineOrders == undefined || medicineOrders.length === 0) {
      toast.error(
        "Замовлення не може містити 0 товарів. Додайте хоча б один товар до замовлення."
      );
      return;
    }

    confirmAlert({
      message: "Ви впевнені, що хочете створити замовлення?",
      title: "Нове замовлення",
      buttons: [
        {
          label: "Так",
          onClick: () => {
            const order: ICreateOrderModel = {
              medicineOrders: medicineOrders.map((item) => {
                return {
                  medicineId: item.medicine.id,
                  count: item.count,
                  price: item.price,
                };
              }),
              customerId: customer ? customer.id : null,
            };

            setOrder(order);
            setVisible(true);
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const clearValues = () => {
    setSearchValue("");
    setFilteredMedicines(storageMedicines);
    setMedicineOrders([]);
  };

  const handleReset = () => {
    confirmAlert({
      title: "Відміна",
      message: "Всі зміни будуть скинуті.",
      buttons: [
        {
          label: "Ок",
          onClick: () => {
            clearValues();
            props.handleLeave();
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const handleClose = () => {
    setVisible(false);
  };

  const handleFindCustomer = (e: any) => {
    if (customerData == undefined || customerData.length < 10) {
      toast.error(
        "Введіть коректні номер телефон або номер бонусної картки клієнта."
      );
      return;
    }

    if (customerData != undefined && customerData.length === 10) {
      customerService
        .getCustomerByCardNumber(customerData)
        .then((response) => {
          setCustomer(response.data);
        })
        .catch((error) =>
          toast.error(
            "Немає такого клієнта. Перевірте дані та спробуйте ще раз."
          )
        );
    } else if (customerData != undefined && customerData.length === 12) {
      customerService
        .getCustomerByPhoneNumber(customerData)
        .then((response) => {
          setCustomer(response.data);
        })
        .catch((error) =>
          toast.error(
            "Немає такого клієнта. Перевірте дані та спробуйте ще раз."
          )
        );
    }
  };

  const calculateTotalSum = () => {
    var sum =
      medicineOrders?.reduce(
        (total, current) => total + current.count * current.medicine?.price,
        0
      ) ?? 0;
    if (customer) {
      sum -= sum * 0.05;
    }

    return sum;
  };

  return (
    <>
      <div className={styles.newOrderContainer}>
        <div className={styles.newOrderSubcontainer}>
          <Navbar
            bg="primary"
            variant="dark"
            style={{ marginTop: 20, marginBottom: 5 }}
          >
            <Form className={styles.navForm}>
              <Form.Label
                style={{
                  flex: 1.5,
                  alignSelf: "center",
                  marginLeft: 5,
                  color: "white",
                  fontWeight: 700,
                }}
              >
                Пошук товарів
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                value={searchValue}
                style={{ margin: 5, flex: 4 }}
                onChange={(e: any) => setSearchValue(e.target.value)}
              />
              <Button
                variant="outline-light"
                style={{ margin: 5, flex: 1 }}
                onClick={handleSearch}
              >
                Пошук
              </Button>
              <Button
                variant="outline-warning"
                style={{ margin: 5, flex: 1 }}
                type="reset"
                onClick={() => {
                  setSearchValue("");
                  setFilteredMedicines(storageMedicines);
                }}
              >
                Очистити
              </Button>
            </Form>
          </Navbar>
          <MedicineTable
            data={filteredMedicines}
            loading={loading}
            handleAddMedicine={handleAddMedicineToOrder}
          />
        </div>
        <div style={{ width: 2, backgroundColor: "black", margin: 15 }} />
        <div className={styles.newOrderSubcontainer}>
          <Container>
            <h3>Оформлення замовлення</h3>
            <Form.Group>
              <Form.Label style={{ fontWeight: 700 }}>Клієнт</Form.Label>
              {!customer ? (
                <div className={styles.customerContainer}>
                  <Form.Control
                    style={{ margin: 4 }}
                    min={10}
                    max={10}
                    placeholder="бонусна картка або номер телефону"
                    value={customerData}
                    onChange={(e: any) => setCustomerData(e.target.value)}
                  ></Form.Control>
                  <Button style={{ margin: 4 }} onClick={handleFindCustomer}>
                    Знайти
                  </Button>
                </div>
              ) : (
                <div className={styles.clientContainer}>
                  <p>
                    {customer.name} {customer.surname}
                  </p>
                  <BsX onClick={() => setCustomer(undefined)} />
                </div>
              )}
            </Form.Group>
          </Container>
          <hr />
          <MedicineOrderTable
            medicineOrders={medicineOrders}
            handleRemoveMedicine={handleRemoveMedicineOrder}
          />
          <hr />
          <p>
            <b>Загальна сума: </b>
            {medicineOrders?.reduce(
              (total, current) =>
                total + current.count * current.medicine?.price,
              0
            ) ?? 0}{" "}
            грн
          </p>
          <p>
            <b>Сума зі знижкою: </b>
            {calculateTotalSum()}
            грн
          </p>
          <div className={styles.buttonContainer}>
            <Button
              variant="primary"
              onClick={handleAddOrder}
              style={{ margin: 10 }}
            >
              Створити
            </Button>
            <Button
              variant="warning"
              onClick={handleReset}
              style={{ margin: 10 }}
            >
              Відмінити
            </Button>
          </div>
        </div>
      </div>
      <Toast />
      <ConfirmationOrder handleClose={handleClose} visible={visible} order={order}/>
    </>
  );
};

export default CreateOrder;
