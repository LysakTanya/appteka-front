import moment from "moment";
import { useState } from "react";
import Table from "../components/Table";
import { BiEditAlt, BiMoney } from "react-icons/bi";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import IUpdateWorkerModel from "./data/IUpdateWorkerModel";
import IWorker from "./data/IWorker";
import workerService from "./data/workerService";
import WorkerModal from "./WorkerModal";
import { GiTeamUpgrade } from "react-icons/gi";
import ChangeRoleModal from "./ChangeRoleModal";
import {AiFillShop} from 'react-icons/ai';
import ChangeStorageModal from './ChangeStorageModal';

interface IWorkerTableProps {
  data: IWorker[];
  loading: boolean;
}

const WorkerTable = (props: IWorkerTableProps) => {
  const [visible, setVisible] = useState(false);
  const [activeWorker, setActiveWorker] = useState<IWorker>({} as IWorker);
  const [changeRoleVisible, setChangeRoleVisible] = useState(false);
  const handleOpenChangeRoleModal = (row: any) => {
    setActiveWorker(row);
    setChangeRoleVisible(true);
  };
  const [changeStorageVisible, setChangeStorageVisible] = useState(false);
  const handleOpenChangeStorageModal = (row: any) => {
    setActiveWorker(row);
    setChangeStorageVisible(true);
  };

  const handleOpen = (row: any) => {
    setActiveWorker(row);
    setVisible(true);
  };

  const handleCloseChangeRoleModal = () => {
    setChangeRoleVisible(false);
  };

  const handleCloseChangeStorageModal = () => {
    setChangeStorageVisible(false);
  };

  const workerColumns = [
    {
      name: "Ім'я",
      selector: "name",
      sortable: true,
    },
    {
      name: "Прізвище",
      selector: "surname",
      sortable: true,
    },
    {
      name: "Дата народження",
      selector: "birthDate",
      sortable: true,
      cell: (row: any) => {
        return <div>{moment(row.birthDate).format("MMM DD, YYYY")}</div>;
      },
    },
    {
      name: "Електронна пошта",
      selector: "email",
      sortable: true,
      grow: 2,
    },
    {
      name: "Номер телефону",
      selector: "phoneNumber",
      sortable: true,
      grow: 2,
    },
    {
      name: "Дата початку роботи",
      selector: "hireDate",
      sortable: true,
      cell: (row: any) => {
        return <div>{moment(row.hireDate).format("MMM DD, YYYY")}</div>;
      },
    },
    {
      name: "Час на посаді",
      selector: "hireDate",
      sortable: false,
      cell: (row: any) => {
        return <div>{moment(row.hireDate).fromNow(true)}</div>;
      },
    },
    {
      name: "Ставка/год",
      selector: "type.salary",
      sortable: false,
      cell: (row: any) => {
        return <div>{row.type.salary} грн</div>;
      },
    },
    {
      name: "Номер торгової точки",
      selector: "storageId",
      sortable: false,
    },
    {
      name: "Роль",
      selector: "type.name",
      sortable: false,
      grow: 2,
    },
    {
      name: "",
      selector: "id",
      sortable: false,
      right: true,
      grow: 2,
      cell: (row: any) => {
        return (
          <>
            <GiTeamUpgrade
              style={{ margin: 4 }}
              size="1.7rem"
              onClick={() => handleOpenChangeRoleModal(row)}
            />
            <AiFillShop style={{ margin: 4 }}
              size="1.7rem"
              onClick={() => handleOpenChangeStorageModal(row)} />
            <BiEditAlt
              style={{ margin: 4 }}
              size="1.7rem"
              onClick={() => handleOpen(row)}
            />
          </>
        );
      },
    },
  ];

  const handleClose = () => {
    setVisible(false);
  };

  const handleUpdateWorker = async (values: any) => {
    const worker: IUpdateWorkerModel = {
      id: values.id,
      typeId: Number(values.typeId),
      name: values.name,
      surname: values.surname,
      birthDate: values.birthDate,
      hireDate: values.hireDate,
      email: values.email,
      phoneNumber: values.phoneNumber,
      storageId: Number(values.storageId),
      password: values.password,
    };

    try {
      let response = await workerService.updateWorker(worker);
      handleClose();
      toast.success(
        `Інформація про працівника ${response.data.name} ${response.data.surname} успішно оновлена.`
      );
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <Table
        data={props.data}
        loading={props.loading}
        title="Працівники"
        columns={workerColumns}
      />
      <WorkerModal
        visible={visible}
        onSubmit={handleUpdateWorker}
        title="Оновити працівника"
        handleClose={handleClose}
        initialValue={activeWorker}
      />
      <ChangeRoleModal
        title={"Оновити посаду працівника "}
        visible={changeRoleVisible}
        handleClose={handleCloseChangeRoleModal}
        initialValue={activeWorker}
      />
      <ChangeStorageModal
        title={"Перевести працівника "}
        visible={changeStorageVisible}
        handleClose={handleCloseChangeStorageModal}
        initialValue={activeWorker}
      />
      <Toast />
    </>
  );
};

export default WorkerTable;
