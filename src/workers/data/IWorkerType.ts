interface IWorkerType {
  id: number;
  name: string;
  salary: number;
}

export default IWorkerType;
