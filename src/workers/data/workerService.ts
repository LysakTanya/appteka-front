import Axios from "../../Axios";
import Routes from "../../Routes";
import ICreateWorkerModel from "./ICreateWorkerModel";
import IUpdateWorkerModel from "./IUpdateWorkerModel";
import IWorker from "./IWorker";

const workerService = {
  getWorkers: () => {
    return Axios.get<IWorker[]>(Routes.workerRoute);
  },

  addWorker: (worker: ICreateWorkerModel) => {
    return Axios.post<IWorker>(Routes.workerRoute, worker);
  },

  updateWorker: (worker: IUpdateWorkerModel) => {
    return Axios.put<IWorker>(Routes.workerRoute, worker);
  },

  updateWorkerRole: (workerId: number, roleId: number) => {
    const updateModel = {
      workerId: Number(workerId),
      roleId: Number(roleId),
    };
    
    return Axios.put<IWorker>(`${Routes.workerRoute}role`, updateModel);
  },

  updateWorkerStorage: (workerId: number, storageId: number) => {
    const updateModel = {
      workerId: Number(workerId),
      storageId: Number(storageId),
    };
    
    return Axios.put<IWorker>(`${Routes.workerRoute}storage`, updateModel);
  },
};

export default workerService;
