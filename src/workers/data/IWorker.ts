import IWorkerType from "./IWorkerType";

interface IWorker {
    id: number;
    name: string;
    surname: string;
    hireDate: Date;
    birthDate: Date;
    type: IWorkerType;
    email: string;
    phoneNumber: string;
    storageId: number;
}

export default IWorker;