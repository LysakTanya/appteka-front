interface IUpdateWorkerModel {
    id: number;
    typeId: number;
    name: string;
    surname: string;
    birthDate: Date;
    hireDate: Date;
    email: string;
    phoneNumber: string;
    storageId: number;
    password: string;
};

export default IUpdateWorkerModel;