import Axios from "../../Axios";
import Routes from "../../Routes";
import IWorkerType from "./IWorkerType";

const workerTypeService = {
  getWorkerTypes: () => Axios.get<IWorkerType[]>(Routes.workerTypeRoute),

  addWorkerType: (workerType: IWorkerType) =>
    Axios.post<IWorkerType>(Routes.workerTypeRoute, workerType),

  updateWorkerType: (workerType: IWorkerType) =>
    Axios.put<IWorkerType>(Routes.workerTypeRoute, workerType),
};

export default workerTypeService;
