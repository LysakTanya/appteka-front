import { Modal, Form, Button } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import IWorkerType from "./data/IWorkerType";
import { FormikErrors, useFormik } from "formik";
import { useEffect } from "react";

interface IWorkerTypeModalProps {
  handleClose: () => void;
  visible: boolean;
  initialValue: IWorkerType;
  onSubmit: (values: any) => Promise<void>;
  title: string;
}

const WorkerTypeModal = (props: IWorkerTypeModalProps) => {
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Назва є обов'язковим полем. ")
      .min(2, "Назва має містити мінімум 2 символи. ")
      .max(50, "Назва може містити максимум 50 символів."),
    salary: yup
      .number()
      .required("Зарплатня є обов'язковим полем. ")
      .moreThan(0, "Зарплатня міє бути більше 0."),
  });

  let formik = useFormik({
    initialValues: { ...props.initialValue },
    validationSchema: schema,
    onSubmit: props.onSubmit,
  });

  useEffect(() => {
    formik.setValues({ ...props.initialValue }, false);
  }, [props.initialValue]);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Назва</Form.Label>
              <Form.Control
                type="text"
                name="name"
                placeholder="Введіть назву"
                defaultValue={formik.values.name}
                value={formik.values.name}
                isValid={formik.touched?.name && !formik.errors.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Зарплатня (ставка за год)</Form.Label>
              <Form.Control
                type="number"
                name="salary"
                placeholder="Введіть зарплатню (за год)"
                defaultValue={formik.values.salary}
                value={formik.values.salary}
                isValid={formik.touched?.salary && !formik.errors.salary}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.salary}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.salary}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default WorkerTypeModal;
