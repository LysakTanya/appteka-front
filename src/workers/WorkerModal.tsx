import React, { useState } from "react";
import { Modal, Form, Button, Col } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { FormikErrors, FormikState, useFormik } from "formik";
import { useEffect } from "react";
import IWorker from "./data/IWorker";
import IStorage from "../storages/data/IStorage";
import storageService from "../storages/data/storageService";
import { toast } from "react-toastify";
import IWorkerType from "./data/IWorkerType";
import workerTypeService from "./data/workerTypeService";
import moment from "moment";
import FormImpl from "react-bootstrap/esm/Form";

interface IWorkerModalProps {
  onSubmit: (values: any) => Promise<void>;
  handleClose: () => void;
  initialValue: IWorker;
  visible: boolean;
  title: string;
}

const WorkerModal = (props: IWorkerModalProps) => {
  const [storages, setStorages] = useState<IStorage[]>([] as IStorage[]);
  const [workerTypes, setWorkerTypes] = useState<IWorkerType[]>(
    [] as IWorkerType[]
  );
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Ім’я є обов'язковим полем. ")
      .min(2, "Ім’я має містити мінімум 2 символи. ")
      .max(50, "Ім’я може містити максимум 50 символів."),
    surname: yup
      .string()
      .min(2, "Прізвище має містити мінімум 2 символи. ")
      .max(50, "Прізвище може містити максимум 50 символів.")
      .required("Прізвище є обов'язковим полем. "),
    email: yup
      .string()
      .email("Введіть коректну електронну пошту.")
      .min(2, "Електронна пошта має містити мінімум 2 символи. ")
      .max(50, "Електронна пошта може містити максимум 50 символів.")
      .required("Електронна пошта є обов'язковим полем. "),
    phoneNumber: yup
      .string()
      .matches(new RegExp("^[+]{1}[0-9]{12}$"), "Некоректний номер телефону")
      .required("Номер телефону є обов'язковим полем. "),
    birthDate: yup.date().required("Дата народження є обов'язковим полем. "),
    hireDate: yup.date().required("Дата найму є обов'язковим полем. "),
    password: yup
      .string()
      .min(8, "Пароль має містити мінімум 8 символів. ")
      .required("Пароль є обов'язковим полем. "),
    typeId: yup.number().required("Посада працівника є обов'язковим полем. "),
    storageId: yup.number().required("Місце роботи є обов'язковим полем. "),
  });

  let formik = useFormik({
    initialValues: {
      id: props.initialValue.id ?? 0,
      name: props.initialValue.name,
      surname: props.initialValue.surname,
      email: props.initialValue.email,
      phoneNumber: props.initialValue.phoneNumber,
      typeId: props.initialValue.type?.id,
      storageId: props.initialValue.storageId,
      birthDate: props.initialValue.birthDate,
      hireDate: props.initialValue.hireDate,
      password: "",
    },
    validationSchema: schema,
    onSubmit: props.onSubmit,
    enableReinitialize: true,
  });

  useEffect(() => {
    formik.setValues(
      {
        id: props.initialValue.id ?? 0,
        name: props.initialValue.name,
        surname: props.initialValue.surname,
        email: props.initialValue.email,
        phoneNumber: props.initialValue.phoneNumber,
        typeId: props.initialValue.type?.id,
        storageId: props.initialValue.storageId,
        birthDate: props.initialValue.birthDate,
        hireDate: props.initialValue.hireDate,
        password: "",
      },
      false
    );
  }, [props.initialValue]);

  useEffect(() => {
    const fetchStorages = async () => {
      try {
        let storagesResponse = await storageService.getAllStorages();
        let workerTypesResponse = await workerTypeService.getWorkerTypes();
        setStorages(storagesResponse.data);
        setWorkerTypes(workerTypesResponse.data);
      } catch (error) {
        toast.error(error.response.data);
      }
    };

    fetchStorages();
  }, []);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Ім’я</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  max={50}
                  min={2}
                  placeholder="Введіть ім’я"
                  defaultValue={formik.values.name}
                  value={formik.values.name}
                  isValid={formik.touched?.name && !formik.errors.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={!!formik.errors.name}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.name}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Прізвище</Form.Label>
                <Form.Control
                  type="text"
                  name="surname"
                  max={50}
                  min={2}
                  placeholder="Введіть прізвище"
                  defaultValue={formik.values.surname}
                  value={formik.values.surname}
                  isValid={formik.touched?.surname && !formik.errors.surname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={!!formik.errors.surname}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.surname}
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Електронна пошта</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  max={50}
                  min={2}
                  placeholder="Введіть електронну пошту"
                  defaultValue={formik.values.email}
                  value={formik.values.email}
                  isValid={formik.touched?.email && !formik.errors.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={!!formik.errors.email}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.email}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Номер телефону</Form.Label>
                <Form.Control
                  type="phone"
                  name="phoneNumber"
                  max={13}
                  placeholder="Введіть номер телефону"
                  defaultValue={formik.values.phoneNumber}
                  value={formik.values.phoneNumber}
                  isValid={
                    formik.touched?.phoneNumber && !formik.errors.phoneNumber
                  }
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={!!formik.errors.phoneNumber}
                />
                <Form.Control.Feedback type="invalid">
                  {formik.errors.phoneNumber}
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Row>
            <Form.Group>
              <Form.Label>Дата народження</Form.Label>
              <Form.Control
                type="date"
                name="birthDate"
                placeholder="Введіть дату народження"
                defaultValue={moment(formik.values.birthDate).format(
                  "YYYY-MM-DD"
                )}
                value={moment(formik.values.birthDate).format("YYYY-MM-DD")}
                isValid={formik.touched?.birthDate && !formik.errors.birthDate}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.birthDate}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.birthDate}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Дата найму</Form.Label>
              <Form.Control
                type="date"
                name="hireDate"
                placeholder="Введіть дату найму"
                defaultValue={moment(formik.values.hireDate).format(
                  "YYYY-MM-DD"
                )}
                value={moment(formik.values.hireDate).format("YYYY-MM-DD")}
                isValid={formik.touched?.hireDate && !formik.errors.hireDate}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.hireDate}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.hireDate}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Пароль</Form.Label>
              <Form.Control
                type="password"
                name="password"
                placeholder="Введіть пароль"
                defaultValue={formik.values.password}
                value={formik.values.password}
                isValid={formik.touched?.password && !formik.errors.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.password}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.password}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Місце роботи</Form.Label>
              <Form.Control
                type="number"
                as="select"
                name="storageId"
                defaultValue={formik.values.storageId}
                value={formik.values.storageId}
                isValid={formik.touched?.storageId && !formik.errors.storageId}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.storageId}
              >
                {storages.map((item) => (
                  <option value={item.id}>
                    {item.id} - {item.city}, {item.street} {item.number}
                  </option>
                ))}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                {formik.errors.storageId}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Посада</Form.Label>
              <Form.Control
                type="number"
                as="select"
                name="typeId"
                defaultValue={formik.values.typeId}
                value={formik.values.typeId}
                isValid={formik.touched?.typeId && !formik.errors.typeId}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.typeId}
              >
                {workerTypes.map((item) => (
                  <option value={item.id}>{item.name}</option>
                ))}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                {formik.errors.storageId}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default WorkerModal;
