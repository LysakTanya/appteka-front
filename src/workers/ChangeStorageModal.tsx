import { useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { useFormik } from "formik";
import { useEffect } from "react";
import IWorker from "./data/IWorker";
import { toast } from "react-toastify";
import IWorkerType from "./data/IWorkerType";
import workerTypeService from "./data/workerTypeService";
import workerService from "./data/workerService";
import IStorage from "../storages/data/IStorage";
import storageService from "../storages/data/storageService";

interface IWorkerModalProps {
  handleClose: () => void;
  initialValue: IWorker;
  visible: boolean;
  title: string;
}

const ChangeStorageModal = (props: IWorkerModalProps) => {
  const [storages, setStorages] = useState<IStorage[]>(
    [] as IStorage[]
  );
  const schema = yup.object().shape({
    storageId: yup.number().required("Місце роботи працівника є обов'язковим полем. "),
  });

  let formik = useFormik({
    initialValues: {
      id: props.initialValue.id,
      storageId: props.initialValue.storageId,
    },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      try {
        let response = await workerService.updateWorkerStorage(
          values.id,
          values.storageId
        );
        toast.success(`Місце роботи працівника успішно оновлене.`);
        props.handleClose();
      } catch (error) {
        toast.error(error.response.data);
      }
    },
    enableReinitialize: true,
  });

  useEffect(() => {
    const fetchWorkerTypes = async () => {
      try {
        let response = await storageService.getAllStorages();
        setStorages(response.data);
      } catch (error) {
        toast.error(error.response.data);
      }
    };

    fetchWorkerTypes();
  });

  useEffect(() => {
    formik.setValues(
      {
        id: props.initialValue.id,
        storageId: props.initialValue.storageId
      },
      false
    );
  }, [props.initialValue]);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>
            {props.title} {props.initialValue.name} {props.initialValue.surname}
          </Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Місце роботи</Form.Label>
              <Form.Control
                type="number"
                as="select"
                name="storageId"
                defaultValue={formik.values.storageId}
                value={formik.values.storageId}
                isValid={formik.touched?.storageId && !formik.errors.storageId}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.storageId}
              >
                {storages.map((item) => (
                  <option value={item.id}>№{item.id} - м. {item.city}, вул. {item.street} буд. {item.number}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ChangeStorageModal;
