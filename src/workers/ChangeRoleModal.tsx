import { useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { useFormik } from "formik";
import { useEffect } from "react";
import IWorker from "./data/IWorker";
import { toast } from "react-toastify";
import IWorkerType from "./data/IWorkerType";
import workerTypeService from "./data/workerTypeService";
import workerService from "./data/workerService";

interface IWorkerModalProps {
  handleClose: () => void;
  initialValue: IWorker;
  visible: boolean;
  title: string;
}

const ChangeRoleModal = (props: IWorkerModalProps) => {
  const [workerTypes, setWorkerTypes] = useState<IWorkerType[]>(
    [] as IWorkerType[]
  );
  const schema = yup.object().shape({
    typeId: yup.number().required("Посада працівника є обов'язковим полем. "),
  });

  let formik = useFormik({
    initialValues: {
      id: props.initialValue.id,
      typeId: props.initialValue.type?.id,
    },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      try {
        let response = await workerService.updateWorkerRole(
          values.id,
          values.typeId
        );
        toast.success(`Посада працівника успішно оновлена.`);
        props.handleClose();
      } catch (error) {
        toast.error(error.response.data);
      }
    },
    enableReinitialize: true,
  });

  useEffect(() => {
    const fetchWorkerTypes = async () => {
      try {
        let response = await workerTypeService.getWorkerTypes();
        setWorkerTypes(response.data);
      } catch (error) {
        toast.error(error.response.data);
      }
    };

    fetchWorkerTypes();
  });

  useEffect(() => {
    formik.setValues(
      {
        id: props.initialValue.id,
        typeId: props.initialValue.type?.id,
      },
      false
    );
  }, [props.initialValue]);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>
            {props.title} {props.initialValue.name} {props.initialValue.surname}
          </Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Посада</Form.Label>
              <Form.Control
                type="number"
                as="select"
                name="typeId"
                defaultValue={formik.values.typeId}
                value={formik.values.typeId}
                isValid={formik.touched?.typeId && !formik.errors.typeId}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.typeId}
              >
                {workerTypes.map((item) => (
                  <option value={item.id}>{item.name}</option>
                ))}
              </Form.Control>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ChangeRoleModal;
