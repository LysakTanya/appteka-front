import React, { useState } from "react";
import DataTable from "react-data-table-component";
import { BiEditAlt } from "react-icons/bi";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import IWorkerType from "./data/IWorkerType";
import workerTypeService from "./data/workerTypeService";
import WorkerTypeModal from "./WorkerTypeModal";
import Table from '../components/Table';

interface IWorkerTypeTableProps {
  data: IWorkerType[];
  loading: boolean;
}

const WorkerTypeTable = (props: IWorkerTypeTableProps) => {
  const [activeWorkerType, setActiveWorkerType] = useState<IWorkerType>(
    {} as IWorkerType
  );
  const [visible, setVisible] = useState(false);
  const handleClose = () => {
    setVisible(false);
  };

  const handleEditClick = (row: any) => {
    const workerType: IWorkerType = {
      id: row.id,
      name: row.name,
      salary: row.salary,
    };

    setActiveWorkerType(workerType);
    setVisible(true);
  };

  const workerTypeColumns = [
    {
      name: "№",
      selector: "id",
      sortable: true,
    },
    {
      name: "Назва",
      selector: "name",
      sortable: true,
    },
    {
      name: "Ставка/год",
      selector: "salary",
      sortable: true,
      cell: (row: any) => <div>{row.salary} грн/год</div>,
    },
    {
      name: "",
      selector: "id",
      sortable: false,
      right: true,
      cell: (row: any) => {
        return <BiEditAlt size="1.7rem" onClick={() => handleEditClick(row)} />;
      },
    },
  ];

  const handleUpdateWorkerType = async (values: any) => {
    const workerType: IWorkerType = {
      id: values.id,
      name: values.name,
      salary: values.salary,
    };

    try {
      let response = await workerTypeService.updateWorkerType(workerType);
      handleClose();
      toast.success(
        `Тип працівника '${response.data.name}' успішно оновлений.`
      );
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <Table
        data={props.data}
        loading={props.loading}
        title="Посади працівників"
        columns={workerTypeColumns}
      />
      <WorkerTypeModal
        initialValue={activeWorkerType}
        handleClose={handleClose}
        title={"Оновити тип працівника"}
        visible={visible}
        onSubmit={handleUpdateWorkerType}
      />
      <Toast />
    </>
  );
};

export default WorkerTypeTable;
