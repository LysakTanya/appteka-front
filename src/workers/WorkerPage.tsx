import React, { useEffect, useState } from "react";
import IWorker from "./data/IWorker";
import { Tabs, Tab, Button } from "react-bootstrap";
import IWorkerType from "./data/IWorkerType";
import { toast } from "react-toastify";
import workerService from "./data/workerService";
import workerTypeService from "./data/workerTypeService";
import WorkerTable from "./WorkerTable";
import WorkerTypeTable from "./WorkerTypeTable";
import WorkerTypeModal from "./WorkerTypeModal";
import WorkerModal from "./WorkerModal";
import ICreateWorkerModel from "./data/ICreateWorkerModel";

const WorkerPage = () => {
  const [workers, setWorkers] = useState<IWorker[]>([] as IWorker[]);
  const [workerModalVisible, setWorkerModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [workerTypes, setWorkerTypes] = useState<IWorkerType[]>(
    [] as IWorkerType[]
  );

  const [workerTypeModalVisible, setWorkerTypeModalVisible] = useState(false);
  const handleWorkerTypeModalClose = () => {
    setWorkerTypeModalVisible(false);
  };

  const handleWorkerModalClose = () => {
    setWorkerModalVisible(false);
  };

  const handleAddWorkerType = async (values: any) => {
    const workerType: IWorkerType = {
      id: 0,
      name: values.name,
      salary: values.salary,
    };

    try {
      let response = await workerTypeService.addWorkerType(workerType);
      toast.success(`Тип працівника '${response.data.name}' успішно доданий.`);
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  const handleAddWorker = async (values: any) => {
    const worker: ICreateWorkerModel = {
      typeId: Number(values.typeId),
      name: values.name,
      surname: values.surname,
      birthDate: values.birthDate,
      hireDate: values.hireDate,
      email: values.email,
      phoneNumber: values.phoneNumber,
      storageId: Number(values.storageId),
      password: values.password,
    };

    try {
      let response = await workerService.addWorker(worker);
      toast.success(
        `Працівник ${response.data.name} ${response.data.surname} успішно доданий.`
      );
      handleWorkerModalClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        let workersResponse = await workerService.getWorkers();
        setWorkers(workersResponse.data);
        let workerTypesResponse = await workerTypeService.getWorkerTypes();
        setWorkerTypes(workerTypesResponse.data);
        setLoading(false);
      } catch (error) {
        toast.error(error.response.data);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      <Tabs defaultActiveKey="workers">
        <Tab eventKey="workers" title="Працівники">
          <Button
            variant="success"
            onClick={() => setWorkerModalVisible(true)}
            style={{ margin: 10 }}
          >
            Додати
          </Button>
          <WorkerTable data={workers} loading={loading} />
          <WorkerModal
            initialValue={{} as IWorker}
            handleClose={handleWorkerModalClose}
            visible={workerModalVisible}
            onSubmit={handleAddWorker}
            title={"Новий працівник"}
          />
        </Tab>
        <Tab eventKey="types" title="Типи працівників">
          <Button
            variant="success"
            onClick={() => setWorkerTypeModalVisible(true)}
            style={{ margin: 10 }}
          >
            Додати
          </Button>
          <WorkerTypeTable data={workerTypes} loading={loading}/>
          <WorkerTypeModal
            title="Додати категорію працівника"
            handleClose={handleWorkerTypeModalClose}
            visible={workerTypeModalVisible}
            initialValue={{} as IWorkerType}
            onSubmit={handleAddWorkerType}
          />
        </Tab>
      </Tabs>
    </>
  );
};

export default WorkerPage;
