import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import AddButton from "../components/AddButton";
import Toast from "../components/Toast";
import CustomerModal from "./CustomerModal";
import CustomerTable from "./CustomerTable";
import customerService from "./data/customerService";
import ICustomer from "./data/ICustomer";

const CustomerPage = () => {
  const [customers, setCustomers] = useState<ICustomer[]>([] as ICustomer[]);
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await customerService.getCustomers();
        setCustomers(response.data);
      } catch (error) {
        toast.error(error.response?.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  const handleAddCustomerClick = () => {
    setVisible(true);
  };

  const handleClose = () => {
    setVisible(false);
  };

  return (
    <>
      <AddButton onClick={handleAddCustomerClick} />
      <CustomerModal visible={visible} handleClose={handleClose} />
      <CustomerTable data={customers} loading={loading} />
      <Toast />
    </>
  );
};

export default CustomerPage;
