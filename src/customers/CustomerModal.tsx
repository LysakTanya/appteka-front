import React from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { useFormik } from "formik";
import moment from "moment";
import ICustomer from "./data/ICustomer";
import { toast } from "react-toastify";
import customerService from "./data/customerService";

interface ICustomerModalProps {
  handleClose: () => void;
  visible: boolean;
}

const CustomerModal = (props: ICustomerModalProps) => {
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Ім’я є обов'язковим полем. ")
      .min(2, "Ім’я має містити мінімум 2 символи. ")
      .max(50, "Ім’я може містити максимум 50 символів."),
    surname: yup
      .string()
      .min(2, "Прізвище має містити мінімум 2 символи. ")
      .max(50, "Прізвище може містити максимум 50 символів.")
      .required("Прізвище є обов'язковим полем. "),
    birthDate: yup.date().required("Дата народження є обов'язковим полем. "),
    phoneNumber: yup
      .string()
      .matches(new RegExp("^[+]{1}[0-9]{12}$"), "Некоректний номер телефону")
      .required("Номер телефону є обов'язковим полем. "),
    cardNumber: yup
      .string()
      .matches(new RegExp("^[0-9]{10}$"), "Номер картки має містити 10 цифр.")
      .required("Номер картки є обов'язковим полем. "),
  });

  let formik = useFormik({
    initialValues: {
      name: "",
      surname: "",
      phoneNumber: "",
      cardNumber: "",
      birthDate: new Date(),
    },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      const customer: ICustomer = {
        id: 0,
        ...values,
      };

      try {
        const response = await customerService.addCustomer(customer);
        toast.success("Клієнт був успішно доданий.");
      } catch (error) {
        toast.error(error.response?.data);
      }
    },
  });

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>Новий клієнт</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Назва</Form.Label>
              <Form.Control
                type="text"
                name="name"
                max={50}
                min={2}
                placeholder="Введіть ім’я"
                defaultValue={formik.values.name}
                value={formik.values.name}
                isValid={formik.touched?.name && !formik.errors.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Прізвище</Form.Label>
              <Form.Control
                type="text"
                name="surname"
                max={50}
                min={2}
                placeholder="Введіть прізвище"
                defaultValue={formik.values.surname}
                value={formik.values.surname}
                isValid={formik.touched?.surname && !formik.errors.surname}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.surname}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.surname}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Номер телефону</Form.Label>
              <Form.Control
                type="phone"
                name="phoneNumber"
                max={13}
                placeholder="Введіть номер телефону"
                defaultValue={formik.values.phoneNumber}
                value={formik.values.phoneNumber}
                isValid={
                  formik.touched?.phoneNumber && !formik.errors.phoneNumber
                }
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.phoneNumber}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.phoneNumber}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Номер бонусної картки</Form.Label>
              <Form.Control
                type="text"
                name="cardNumber"
                max={10}
                min={10}
                placeholder="0000000000"
                defaultValue={formik.values.cardNumber}
                value={formik.values.cardNumber}
                isValid={
                  formik.touched?.cardNumber && !formik.errors.cardNumber
                }
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.cardNumber}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.cardNumber}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Дата народження</Form.Label>
              <Form.Control
                type="date"
                min={moment("1900-01-01").format("yyyy-MM-DD")}
                max={moment().format("yyyy-MM-DD")}
                name="birthDate"
                placeholder="Введіть дату народження"
                defaultValue={moment(formik.values.birthDate).format(
                  "yyyy-MM-DD"
                )}
                value={moment(formik.values.birthDate).format("yyyy-MM-DD")}
                isValid={formik.touched?.birthDate && !formik.errors.birthDate}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.birthDate}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.birthDate}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default CustomerModal;
