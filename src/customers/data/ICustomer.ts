interface ICustomer {
  id: number;
  name: string;
  surname: string;
  cardNumber: string;
  birthDate: Date;
  phoneNumber: string;
}

export default ICustomer;
