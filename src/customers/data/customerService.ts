import Axios from "../../Axios";
import Routes from "../../Routes";
import ICustomer from "./ICustomer";

const customerService = {
  getCustomers: async () => Axios.get<ICustomer[]>(Routes.customersRoute),

  addCustomer: async (customer: ICustomer) =>
    Axios.post<ICustomer>(Routes.customersRoute, customer),

  getCustomerByCardNumber: async (cardNumber: string) =>
    Axios.get<ICustomer>(`${Routes.customersRoute}card/${cardNumber}`),

  getCustomerByPhoneNumber: async (phoneNumber: string) =>
    Axios.get<ICustomer>(`${Routes.customersRoute}phone/${phoneNumber}`),
};

export default customerService;
