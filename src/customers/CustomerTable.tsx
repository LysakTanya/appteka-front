import moment from "moment";
import Table from "../components/Table";
import Toast from "../components/Toast";
import ICustomer from "./data/ICustomer";

interface ICustomerTableProps {
  data: ICustomer[];
  loading: boolean;
}

const CustomerTable = (props: ICustomerTableProps) => {
  const columns = [
    {
      name: "Номер картки",
      selector: "cardNumber",
      sortable: true,
    },
    {
      name: "Ім’я",
      selector: "name",
      sortable: true,
    },
    {
      name: "Прізвище",
      selector: "surname",
      sortable: true,
    },
    {
      name: "Номер телефону",
      selector: "phoneNumber",
      sortable: true,
    },
    {
      name: "Дата народження",
      selector: "birthDate",
      sortable: true,
      cell: (row: any) => {
        return <div>{moment(row.birthDate).format("MMM DD, YYYY")}</div>;
      },
    },
  ];

  return (
    <>
      <Table
        data={props.data}
        loading={props.loading}
        title="Клієнти"
        columns={columns}
      />
      <Toast />
    </>
  );
};

export default CustomerTable;
