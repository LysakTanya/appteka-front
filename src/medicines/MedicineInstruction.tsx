import React from "react";
import { Card, Accordion } from "react-bootstrap";
import IInstruction from "./data/IInstruction";

const MedicineInstruction = ({ instruction }: any) => {
  return (
    <Card>
      <Accordion.Toggle as={Card.Header} variant="link" eventKey="0">
        Правила застосування:
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="0">
        <Card.Body>{instruction?.properUse}</Card.Body>
      </Accordion.Collapse>
      <Accordion.Toggle as={Card.Header} variant="link" eventKey="1">
        Дозування:
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="1">
        <Card.Body>{instruction?.dosing}</Card.Body>
      </Accordion.Collapse>
      <Accordion.Toggle as={Card.Header} variant="link" eventKey="2">
        Взаємодії:
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="2">
        <Card.Body>{instruction?.interactions}</Card.Body>
      </Accordion.Collapse>
      <Accordion.Toggle as={Card.Header} variant="link" eventKey="3">
        Застереження:
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="3">
        <Card.Body>{instruction?.precautions}</Card.Body>
      </Accordion.Collapse>
      <Accordion.Toggle as={Card.Header} variant="link" eventKey="4">
        Побічні дії:
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="4">
        <Card.Body>{instruction?.sideEffects}</Card.Body>
      </Accordion.Collapse>
      <Accordion.Toggle as={Card.Header} variant="link" eventKey="5">
        Правила зберігання:
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="5">
        <Card.Body>{instruction?.storage}</Card.Body>
      </Accordion.Collapse>
    </Card>
  );
};

export default MedicineInstruction;
