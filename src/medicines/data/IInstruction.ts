interface IInstruction {
  id?: number;
  interactions: string;
  dosing: string;
  storage: string;
  precautions: string;
  sideEffects: string;
  properUse: string;
}

export default IInstruction;
