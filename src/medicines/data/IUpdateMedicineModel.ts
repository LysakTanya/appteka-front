import ICategory from "./ICategory";
import IInstruction from "./IInstruction";

interface IUpdateMedicineModel {
  id: number;
  name: string;
  isActual: boolean;
  quantity: number;
  code: string;
  price: number;
  categoryId: number;
  instruction: IInstruction | null;
}

export default IUpdateMedicineModel;
