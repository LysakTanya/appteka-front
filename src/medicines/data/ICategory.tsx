interface ICategory {
  id: number;
  name: string;
  measure: string;
}

export default ICategory;
