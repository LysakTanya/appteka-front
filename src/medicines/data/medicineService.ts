import axios from "axios";
import IMedicine from "./IMedicine";
import Routes from "../../Routes";
import ICreateMedicineModel from "./ICreateMedicineModel";
import Axios from "../../Axios";
import IInstruction from "./IInstruction";
import IUpdateMedicineModel from "./IUpdateMedicineModel";

const medicineService = {
  getMedicineById: async (medicineId: number) =>
    Axios.get<IMedicine>(Routes.medicineRoute + medicineId),

  getMedicines: async () => Axios.get<IMedicine[]>(Routes.medicineRoute),

  addMedicine: async (medicine: ICreateMedicineModel) =>
    Axios.post<IMedicine>(Routes.medicineRoute, medicine),

  addInstruction: async (instruction: IInstruction) => {
    return Axios.put<IInstruction>(
      Routes.medicineRoute + "instructions/",
      instruction
    );
  },

  updateMedicine: async(medicine: IUpdateMedicineModel) => {
    return Axios.put<IMedicine>(Routes.medicineRoute, medicine);
  }
};

export default medicineService;
