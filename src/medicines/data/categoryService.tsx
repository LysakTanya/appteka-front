import Routes from "../../Routes";
import Axios from "../../Axios";
import ICategory from "./ICategory";

const categoryService = {
  getCategories: async () => Axios.get<ICategory[]>(Routes.categoryRoute),

  addCategory: async (category: ICategory) =>
    Axios.post<ICategory>(Routes.categoryRoute, category),

  updateCategory: async (category: ICategory) =>
    Axios.put<ICategory>(Routes.categoryRoute, category),
};

export default categoryService;
