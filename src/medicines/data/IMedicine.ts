import ICategory from "./ICategory";
import IInstruction from "./IInstruction";

interface IMedicine {
  id: number;
  name: string;
  isActual: boolean;
  quantity: number;
  code: string;
  price: number;
  category: ICategory;
  instruction: IInstruction | null;
}

export default IMedicine;
