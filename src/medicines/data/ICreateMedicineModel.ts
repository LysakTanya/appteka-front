import IInstruction from "./IInstruction";

interface ICreateMedicineModel {
    name: string;
    isActual: boolean;
    quantity: number;
    code: string;
    price: number;
    categoryId: number;
    instruction: IInstruction | null;
  };
  
  export default ICreateMedicineModel;