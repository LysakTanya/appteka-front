import { useEffect, useState } from "react";
import { Button, Col, Row, Modal, Form, Card } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import ICategory from "./data/ICategory";
import * as yup from "yup";
import { useFormik } from "formik";
import IMedicine from "./data/IMedicine";

interface IMedicineModalProps {
  visible: boolean;
  handleClose: () => void;
  categories: ICategory[];
  initialValue: IMedicine;
  onSubmit: (values: any) => Promise<void>;
  changeHasInstruction: (hasInstruction: boolean) => void;
  hasInstruction: boolean;
}

const MedicineFormModal = (props: IMedicineModalProps) => {
  const [actual, setActual] = useState(false);
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Назва є обов'язковим полем. ")
      .min(2, "Назва має містити мінімум 2 символи. ")
      .max(50, "Назва може містити максимум 50 символів."),
    quantity: yup
      .number()
      .moreThan(0, "Кількість має бути більшою за 0.")
      .required("Кількість є обов'язковим полем. "),
    price: yup
      .number()
      .moreThan(0, "Ціна має бути більшою за 0.")
      .required("Ціна є обов'язковим полем. "),
    categoryId: yup.number().required("Категорія є обов'язковим полем. "),
    code: yup
      .string()
      .required("Код є обов'язковим полем. ")
      .matches(new RegExp("^[0-9]{13}$"), "Код має містити 13 цифр."),
    properUse: yup.string().max(2000),
    sideEffects: yup.string().max(2000),
    storage: yup.string().max(2000),
    dosing: yup.string().max(2000),
    interactions: yup.string().max(2000),
    precautions: yup.string().max(2000),
  });

  const formik = useFormik({
    initialValues: {
      id: props.initialValue.id ?? 0,
      name: props.initialValue.name,
      price: props.initialValue.price,
      categoryId: props.initialValue.category?.id ?? 1,
      isActual: props.initialValue.isActual,
      quantity: props.initialValue.quantity,
      code: props.initialValue.code,
      properUse: props.initialValue.instruction?.properUse,
      precautions: props.initialValue.instruction?.precautions,
      storage: props.initialValue.instruction?.storage,
      sideEffects: props.initialValue.instruction?.sideEffects,
      dosing: props.initialValue.instruction?.dosing,
      interactions: props.initialValue.instruction?.interactions,
    },
    validationSchema: schema,
    onSubmit: props.onSubmit,
  });

  useEffect(() => {
    setActual(props.initialValue.isActual);
    formik.setValues(
      {
        id: props.initialValue.id ?? 0,
        name: props.initialValue.name,
        price: props.initialValue.price,
        categoryId: props.initialValue.category?.id,
        isActual: props.initialValue.isActual,
        quantity: props.initialValue.quantity,
        code: props.initialValue.code,
        properUse: props.initialValue.instruction?.properUse,
        precautions: props.initialValue.instruction?.precautions,
        storage: props.initialValue.instruction?.storage,
        sideEffects: props.initialValue.instruction?.sideEffects,
        dosing: props.initialValue.instruction?.dosing,
        interactions: props.initialValue.instruction?.interactions,
      },
      false
    );
  }, [props.initialValue]);

  const NoInstructionView = () => (
    <Card>
      <Card.Header>Цей препарат не має інструкції.</Card.Header>
      <Card.Body>
        <Button
          variant="primary"
          onClick={() => {
            props.changeHasInstruction(true);
          }}
        >
          Додати інструкцію
        </Button>
      </Card.Body>
    </Card>
  );

  return (
    <>
      <Modal
        show={props.visible}
        size="xl"
        onHide={() => {
          props.handleClose();
        }}
        backdrop="static"
      >
        <>
          <Modal.Header>
            <Modal.Title>Препарат</Modal.Title>
            <BsX onClick={props.handleClose} />
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Form onSubmit={formik.handleSubmit} style={{ margin: 5 }}>
                <Form.Group>
                  <Form.Label>Назва: </Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    defaultValue={formik.values.name}
                    value={formik.values.name}
                    isValid={formik.touched?.name && !formik.errors.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    isInvalid={!!formik.errors.name}
                  />
                  <Form.Control.Feedback type="invalid">
                    {formik.errors.name}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Код товару: </Form.Label>
                  <Form.Control
                    type="text"
                    maxLength={13}
                    name="code"
                    defaultValue={formik.values.code}
                    value={formik.values.code}
                    onBlur={formik.handleBlur}
                    isValid={formik.touched?.code && !formik.errors.code}
                    isInvalid={!!formik.errors.code}
                    onChange={formik.handleChange}
                  />
                  <Form.Control.Feedback type={"invalid"}>
                    {formik.errors.code}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="checkbox"
                    label={`актуальний`}
                    name="isActual"
                    defaultChecked={formik.values.isActual}
                    checked={formik.values.isActual}
                    value={String(formik.values.isActual)}
                    onChange={(e: any) => {
                      formik.setFieldValue("isActual", e.target.checked);
                      console.log(formik.values);
                      console.log(formik.values);
                      console.log(formik.values);
                    }}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Кількість: </Form.Label>
                  <Form.Control
                    type="number"
                    name="quantity"
                    defaultValue={formik.values.quantity}
                    value={formik.values.quantity}
                    onBlur={formik.handleBlur}
                    isValid={
                      formik.touched?.quantity && !formik.errors.quantity
                    }
                    isInvalid={!!formik.errors.quantity}
                    onChange={formik.handleChange}
                  />
                  <Form.Control.Feedback type={"invalid"}>
                    {formik.errors.quantity}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Ціна (в грн): </Form.Label>
                  <Form.Control
                    type="number"
                    name="price"
                    defaultValue={formik.values.price}
                    value={formik.values.price}
                    onBlur={formik.handleBlur}
                    isValid={formik.touched?.price && !formik.errors.price}
                    isInvalid={!!formik.errors.price}
                    onChange={formik.handleChange}
                  />
                  <Form.Control.Feedback type={"invalid"}>
                    {formik.errors.price}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Категорія: </Form.Label>
                  <Form.Control
                    as="select"
                    name="categoryId"
                    defaultValue={String(formik.values.categoryId)}
                    value={String(formik.values.categoryId)}
                    onBlur={formik.handleBlur}
                    isValid={
                      formik.touched?.categoryId && !formik.errors.categoryId
                    }
                    isInvalid={!!formik.errors.categoryId}
                    onChange={formik.handleChange}
                  >
                    {props.categories?.map((item) => (
                      <option value={item.id}>
                        {item.name} ({item.measure})
                      </option>
                    ))}
                  </Form.Control>
                  <Form.Control.Feedback type={"invalid"}>
                    {formik.errors.categoryId}
                  </Form.Control.Feedback>
                </Form.Group>
                {props.hasInstruction ? (
                  <>
                    <Form.Group>
                      <Form.Label>Правила застосування</Form.Label>
                      <Form.Control
                        as={"textarea"}
                        name="properUse"
                        defaultValue={formik.values.properUse}
                        value={formik.values.properUse}
                        onBlur={formik.handleBlur}
                        isValid={
                          formik.touched?.properUse && !formik.errors.properUse
                        }
                        isInvalid={!!formik.errors.properUse}
                        onChange={formik.handleChange}
                      />
                      <Form.Control.Feedback type={"invalid"}>
                        {formik.errors.properUse}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Дозування</Form.Label>
                      <Form.Control
                        as={"textarea"}
                        name="dosing"
                        defaultValue={formik.values.dosing}
                        value={formik.values.dosing}
                        onBlur={formik.handleBlur}
                        isValid={
                          formik.touched?.dosing && !formik.errors.dosing
                        }
                        isInvalid={!!formik.errors.dosing}
                        onChange={formik.handleChange}
                      />
                      <Form.Control.Feedback type={"invalid"}>
                        {formik.errors.dosing}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Взаємодії</Form.Label>
                      <Form.Control
                        as={"textarea"}
                        name="interactions"
                        defaultValue={formik.values.interactions}
                        value={formik.values.interactions}
                        onBlur={formik.handleBlur}
                        isValid={
                          formik.touched?.interactions &&
                          !formik.errors.interactions
                        }
                        isInvalid={!!formik.errors.interactions}
                        onChange={formik.handleChange}
                      />
                      <Form.Control.Feedback type={"invalid"}>
                        {formik.errors.interactions}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Побічні реакції</Form.Label>
                      <Form.Control
                        as={"textarea"}
                        name="sideEffects"
                        defaultValue={formik.values.sideEffects}
                        value={formik.values.sideEffects}
                        onBlur={formik.handleBlur}
                        isValid={
                          formik.touched?.sideEffects &&
                          !formik.errors.sideEffects
                        }
                        isInvalid={!!formik.errors.sideEffects}
                        onChange={formik.handleChange}
                      />
                      <Form.Control.Feedback type={"invalid"}>
                        {formik.errors.sideEffects}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Застереження</Form.Label>
                      <Form.Control
                        as={"textarea"}
                        name="precautions"
                        defaultValue={formik.values.precautions}
                        value={formik.values.precautions}
                        onBlur={formik.handleBlur}
                        isValid={
                          formik.touched?.precautions &&
                          !formik.errors.precautions
                        }
                        isInvalid={!!formik.errors.precautions}
                        onChange={formik.handleChange}
                      />
                      <Form.Control.Feedback type={"invalid"}>
                        {formik.errors.precautions}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Правила зберігання</Form.Label>
                      <Form.Control
                        as={"textarea"}
                        name="storage"
                        defaultValue={formik.values.storage}
                        value={formik.values.storage}
                        onBlur={formik.handleBlur}
                        isValid={
                          formik.touched?.storage && !formik.errors.storage
                        }
                        isInvalid={!!formik.errors.storage}
                        onChange={formik.handleChange}
                      />
                      <Form.Control.Feedback type={"invalid"}>
                        {formik.errors.storage}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Row
                      className="justify-content-md-center"
                      style={{ margin: 5 }}
                    >
                      <Col xs lg="2">
                        <Button
                          variant="outline-warning"
                          onClick={() => props.changeHasInstruction(false)}
                        >
                          Не додавати інструкцію
                        </Button>
                      </Col>
                    </Row>
                  </>
                ) : (
                  <NoInstructionView />
                )}
                <Button type="submit">Зберегти</Button>
              </Form>
            </Row>
          </Modal.Body>
        </>
      </Modal>
    </>
  );
};

export default MedicineFormModal;
