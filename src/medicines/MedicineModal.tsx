import { useState } from "react";
import IMedicine from "./data/IMedicine";
import {
  Badge,
  Button,
  Col,
  Row,
  Modal,
  Accordion,
  Card,
} from "react-bootstrap";
import { BsX } from "react-icons/bs";
import MedicineInstruction from "./MedicineInstruction";
import InstructionModal from "./InstructionModal";
import AddButton from '../components/AddButton';

interface IMedicineModalProps {
  data: IMedicine;
  visible: boolean;
  handleClose: () => void;
}

const MedicineModal = (props: IMedicineModalProps) => {
  const { data } = props;
  const [instructionVisible, setInstructionVisible] = useState(false);
  const handleInstructionFormOpen = () => {
    setInstructionVisible(true);
    props.handleClose();
  };

  const handleInstructionFormClose = () => {
    setInstructionVisible(false);
  };

  const NoInstructionView = () => (
    <Card>
      <Card.Header>Цей препарат не має інструкції.</Card.Header>
      <Card.Body>
        <AddButton onClick={handleInstructionFormOpen} />
      </Card.Body>
    </Card>
  );

  return (
    <>
      <Modal
        show={props.visible}
        size="lg"
        onHide={() => {
          props.handleClose();
        }}
        backdrop="static"
      >
        <>
          <Modal.Header>
            <Modal.Title>
              {data?.name}{" "}
              <Badge pill style={{ background: "#0d6efd" }}>
                {data?.category?.name}
              </Badge>
            </Modal.Title>
            <BsX onClick={props.handleClose} />
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col>
                <h4>Код товару: {data?.code}</h4>
                <p>
                  <b>Ціна:</b> {data?.price} грн
                </p>
                <p>
                  <b>Кількість:</b> {data?.quantity} {data?.category?.measure}
                </p>
                <p>
                  <b>Актуальний:</b> {data?.isActual ? "Так" : "Ні"}
                </p>
                <p>
                  <b>Категорія:</b> {data?.category?.name}
                </p>
              </Col>
              <Col xs={7}>
                <Accordion defaultActiveKey="0">
                  <h3>Інструкція</h3>
                  {data?.instruction ? (
                    <MedicineInstruction instruction={data?.instruction} />
                  ) : (
                    <>
                      <NoInstructionView />
                    </>
                  )}
                </Accordion>
              </Col>
            </Row>
          </Modal.Body>
        </>
      </Modal>
      <InstructionModal
        medicine={data}
        visible={instructionVisible}
        handleClose={handleInstructionFormClose}
      />
    </>
  );
};

export default MedicineModal;
