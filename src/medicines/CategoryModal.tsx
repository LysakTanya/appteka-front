import { FormikErrors, useFormik } from "formik";
import { useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import * as yup from "yup";
import ICategory from "./data/ICategory";

interface ICategoryModalProps {
  visible: boolean;
  handleClose: () => void;
  title: string;
  onSubmit: (values: any) => Promise<void>;
  initialValue: ICategory;
}

const CategoryModal = (props: ICategoryModalProps) => {
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Назва є обов'язковим полем. ")
      .min(2, "Назва має містити мінімум 2 символи. ")
      .max(50, "Назва може містити максимум 50 символів."),
    measure: yup
      .string()
      .required("Міра є обов'язковим полем. ")
      .max(20, " Міра може містити максимум 20 символів."),
  });

  let formik = useFormik({
    initialValues: {
      id: props.initialValue.id,
      name: props.initialValue.name,
      measure: props.initialValue.measure,
    },
    validationSchema: schema,
    onSubmit: props.onSubmit,
  });

  useEffect(() => {
    formik.setValues({ ...props.initialValue });
    formik.setErrors({} as FormikErrors<any>);
  }, [props.initialValue]);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Назва</Form.Label>
              <Form.Control
                type="text"
                name="name"
                placeholder="Введіть назву"
                defaultValue={formik.values.name}
                isValid={formik.touched?.name && !formik.errors.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Міра</Form.Label>
              <Form.Control
                type="text"
                name="measure"
                placeholder="Введіть міру"
                defaultValue={formik.values.measure}
                isValid={formik.touched?.measure && !formik.errors.measure}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.measure}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.measure}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default CategoryModal;
