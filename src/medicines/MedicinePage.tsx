import { useState, useEffect } from "react";
import medicineService from "./data/medicineService";
import {
  Button,
  Form,
  Toast,
  FormControl,
  Navbar,
  Tab,
  Tabs,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import MedicineTable from "./MedicineTable";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import MedicineFormModal from "./MedicineFormModal";
import IMedicine from "./data/IMedicine";
import ICategory from "./data/ICategory";
import categoryService from "./data/categoryService";
import CategoryTable from "./CategoryTable";
import CategoryModal from "./CategoryModal";
import ICreateMedicineModel from "./data/ICreateMedicineModel";
import IInstruction from "./data/IInstruction";
import AddButton from "../components/AddButton";

const MedicinePage = () => {
  const [medicines, setMedicines] = useState<IMedicine[]>([] as IMedicine[]);
  const [filteredMedicines, setFilteredMedicines] = useState<IMedicine[]>(
    [] as IMedicine[]
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [medicineModalVisible, setMedicineModalVisible] = useState(false);

  const [categoryFilters, setCategoryFilters] = useState<string[]>(
    [] as string[]
  );
  const [searchValue, setSearchValue] = useState<string>("");

  const [categories, setCategories] = useState<ICategory[]>([] as ICategory[]);
  const [categoryModalVisible, setCategoryModalVisible] = useState(false);

  const [hasInstruction, setHasInstruction] = useState(false);
  const changeHasInstruction = (hasInstruction: boolean) => {
    setHasInstruction(hasInstruction);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        let medicinesResponse = await medicineService.getMedicines();
        let categoriesResponse = await categoryService.getCategories();

        setMedicines(medicinesResponse.data);
        setFilteredMedicines(medicinesResponse.data);

        setCategories(categoriesResponse.data);

        setLoading(false);
      } catch (error) {
        setLoading(false);
        toast.error("Помилка при завантаженні даних.");
      }
    };

    fetchData();
  }, []);

  const openMedicineModal = () => {
    setMedicineModalVisible(true);
  };

  const handleCloseMedicineModal = () => {
    setMedicineModalVisible(false);
  };

  const handleUpdateMedicines = (medicine: IMedicine) => {
    const filteredMedicines = medicines.filter(
      (item) => item.id !== medicine.id
    );
    setMedicines([...filteredMedicines!, medicine]);
  };

  const applyFilters = () => {
    const filtered = medicines.filter((item) => {
      return (
        (searchValue === "" ||
          item.name.match(new RegExp(`${searchValue}`, "i")) ||
          item.code.includes(searchValue)) &&
        (categoryFilters.includes(item.category.id.toString()) ||
          categoryFilters.length === 0)
      );
    });

    setFilteredMedicines(filtered);
  };

  const resetFilters = () => {
    setCategoryFilters([] as string[]);
    setFilteredMedicines(medicines);
    setSearchValue("");
  };

  const handleAddCategoryClick = () => {
    setCategoryModalVisible(true);
  };

  const handleCloseCategoryModal = () => {
    setCategoryModalVisible(false);
  };

  const onAddSubmit = async (values: any) => {
    const newCategory: ICategory = {
      id: 0,
      name: values.name,
      measure: values.measure,
    };

    try {
      let response = await categoryService.addCategory(newCategory);
      handleCloseCategoryModal();
      toast.success(`Категорія '${response.data.name}' успішно додана.`);
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  const handleAddMedicine = async (values: any) => {
    console.log(values);
    const medicine: ICreateMedicineModel = {
      name: values.name,
      isActual: Boolean(values.isActual),
      quantity: values.quantity,
      price: values.price,
      categoryId: Number(values.categoryId),
      code: values.code,
      instruction: hasInstruction
        ? ({
            id: 0,
            properUse: values.properUse,
            precautions: values.precautions,
            storage: values.storage,
            sideEffects: values.sideEffects,
            dosing: values.dosing,
            interactions: values.interactions,
          } as IInstruction)
        : null,
    };

    try {
      let response = await medicineService.addMedicine(medicine);
      toast.success(`Препарат '${response.data.name}' був успішно доданий.`);
      handleCloseMedicineModal();
      setTimeout(() => window.location.reload(), 3000);
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <Tabs defaultActiveKey="medicines">
        <Tab eventKey="medicines" title="Препарати">
          <AddButton onClick={openMedicineModal} />
          <br />
          <form
            style={{
              flexDirection: "row",
              display: "inline-block",
              justifyContent: "space-around",
              alignItems: "flex-start",
            }}
          >
            <Navbar bg="light" expand="lg">
              <Navbar.Brand style={{ marginLeft: 10, marginTop: 0 }}>
                Пошук
              </Navbar.Brand>
              <div
                style={{
                  margin: 10,
                }}
              >
                {categories?.map((item) => (
                  <Form.Check
                    inline
                    label={item.name}
                    type={"checkbox"}
                    value={item.id}
                    onChange={(e: any) => {
                      if (e.target.checked) {
                        setCategoryFilters(
                          categoryFilters.concat(e.target.value)
                        );
                      } else {
                        setCategoryFilters(
                          categoryFilters.filter(
                            (item) => item !== e.target.value
                          )
                        );
                      }
                    }}
                  />
                ))}
                <div
                  style={{
                    display: "inline-flex",
                    margin: 10,
                  }}
                >
                  <FormControl
                    type="text"
                    placeholder="Search"
                    onChange={(e: any) => {
                      setSearchValue(e.target.value);
                    }}
                  />

                  <Button
                    variant="primary"
                    style={{ marginLeft: 5 }}
                    onClick={() => applyFilters()}
                  >
                    Шукати
                  </Button>
                  <Button
                    style={{ marginLeft: 5 }}
                    type="reset"
                    variant="warning"
                    onClick={() => resetFilters()}
                  >
                    Скинути
                  </Button>
                </div>
              </div>
            </Navbar>
          </form>
          <MedicineTable
            medicines={filteredMedicines}
            categories={categories}
            loading={loading}
          />
          <MedicineFormModal
            visible={medicineModalVisible}
            handleClose={handleCloseMedicineModal}
            categories={categories}
            initialValue={{} as IMedicine}
            onSubmit={handleAddMedicine}
            hasInstruction={hasInstruction}
            changeHasInstruction={changeHasInstruction}
          />
        </Tab>
        <Tab eventKey="categories" title="Категорії товарів">
          <AddButton onClick={handleAddCategoryClick} />
          <CategoryTable categories={categories} />
          <CategoryModal
            handleClose={handleCloseCategoryModal}
            visible={categoryModalVisible}
            initialValue={{} as ICategory}
            onSubmit={onAddSubmit}
            title="Нова категорія"
          />
        </Tab>
      </Tabs>
      <Toast />
    </>
  );
};

export default MedicinePage;
