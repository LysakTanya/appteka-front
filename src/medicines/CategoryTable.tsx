import React, { useState } from "react";
import { useEffect } from "react";
import DataTable from "react-data-table-component";
import { toast } from "react-toastify";
import categoryService from "./data/categoryService";
import ICategory from "./data/ICategory";
import { BiEditAlt } from "react-icons/bi";
import CategoryModal from "./CategoryModal";
import Toast from "../components/Toast";

const CategoryTable = ({ categories }: any) => {
  const [visible, setVisible] = useState(false);
  const [activeCategory, setActiveCategory] = useState<ICategory>(
    {} as ICategory
  );
  const handleEditClick = (row: any) => {
    const category: ICategory = {
      id: row.id,
      name: row.name,
      measure: row.measure,
    };
    setActiveCategory(category);
    setVisible(true);
  };

  const handleClose = () => {
    setVisible(false);
  };

  const columns = [
    {
      name: "Номер",
      selector: "id",
      sortable: true,
    },
    {
      name: "Назва",
      selector: "name",
      sortable: true,
    },
    {
      name: "Міра",
      selector: "measure",
      sortable: false,
    },
    {
      name: "",
      selector: "category",
      sortable: false,
      cell: (row: any) => {
        return <BiEditAlt size="1.7rem" onClick={() => handleEditClick(row)} />;
      },
    },
  ];

  const handleSubmitEdit = async (values: any) => {
    const category: ICategory = {
      id: values.id,
      name: values.name,
      measure: values.measure,
    };

    try {
      let response = await categoryService.updateCategory(category);
      toast.success("Категорія успішно оновлена.");
      handleClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <DataTable
        title="Категорії товарів"
        columns={columns}
        data={categories}
      ></DataTable>
      <CategoryModal
        title="Оновити категорію"
        visible={visible}
        handleClose={handleClose}
        onSubmit={handleSubmitEdit}
        initialValue={activeCategory}
      />
      <Toast />
    </>
  );
};

export default CategoryTable;
