import { useEffect } from "react";
import { Modal, Form, Button, Badge, Col, Row } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import { toast } from "react-toastify";
import IInstruction from "./data/IInstruction";
import IMedicine from "./data/IMedicine";
import medicineService from "./data/medicineService";
import * as yup from "yup";
import { useFormik } from "formik";
import Toast from "../components/Toast";

interface IInstructionModalProps {
  medicine: IMedicine;
  visible: boolean;
  handleClose: () => void;
}

const InstructionModal = ({
  medicine,
  visible,
  handleClose,
}: IInstructionModalProps) => {
  const schema = yup.object().shape({
    properUse: yup
      .string()
      .required("Правила використання є обов’язковим полем.")
      .max(2000, "Поле може містити максимум 2000 символів."),
    sideEffects: yup
      .string()
      .required("Побічні ефекти є обов’язковим полем.")
      .max(2000, "Поле може містити максимум 2000 символів."),
    storage: yup
      .string()
      .required("Правила зберігання є обов’язковим полем.")
      .max(2000, "Поле може містити максимум 2000 символів."),
    dosing: yup
      .string()
      .required("Дозування є обов’язковим полем.")
      .max(2000, "Поле може містити максимум 2000 символів."),
    interactions: yup
      .string()
      .required("Взаємодії є обов’язковим полем.")
      .max(2000, "Поле може містити максимум 2000 символів."),
    precautions: yup
      .string()
      .required("Протипоказання є обов’язковим полем.")
      .max(2000, "Поле може містити максимум 2000 символів."),
  });

  const handleAddInstruction = async (values: any) => {
    const instruction: IInstruction = {
      id: medicine?.id,
      properUse: values.properUse,
      precautions: values.precautions,
      storage: values.storage,
      sideEffects: values.sideEffects,
      dosing: values.dosing,
      interactions: values.interactions,
    };

    try {
      let response = await medicineService.addInstruction(instruction);
      toast.success("Інструкція була успішно додана.");
      handleClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  let formik = useFormik({
    initialValues: {
      id: medicine?.id,
      properUse: medicine?.instruction?.properUse,
      precautions: medicine?.instruction?.precautions,
      storage: medicine?.instruction?.storage,
      sideEffects: medicine?.instruction?.sideEffects,
      dosing: medicine?.instruction?.dosing,
      interactions: medicine?.instruction?.interactions,
    },
    validationSchema: schema,
    onSubmit: handleAddInstruction,
  });

  useEffect(() => {
    formik.setValues(
      {
        id: medicine.id,
        properUse: medicine.instruction?.properUse,
        precautions: medicine.instruction?.precautions,
        storage: medicine.instruction?.storage,
        sideEffects: medicine.instruction?.sideEffects,
        dosing: medicine.instruction?.dosing,
        interactions: medicine.instruction?.interactions,
      },
      false
    );
  }, [medicine]);

  return (
    <>
      <Modal show={visible} size="lg">
        <Modal.Header>
          <Modal.Title>
            Інструкція для {medicine?.name} -{" "}
            <Badge style={{ backgroundColor: "#17a2b7" }}>
              {medicine.code}
            </Badge>
          </Modal.Title>
          <BsX onClick={handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 5 }}>
            <Form.Group>
              <Form.Label>Правила застосування</Form.Label>
              <Form.Control
                as={"textarea"}
                name="properUse"
                defaultValue={formik.values.properUse}
                value={formik.values.properUse}
                onBlur={formik.handleBlur}
                isValid={formik.touched?.properUse && !formik.errors.properUse}
                isInvalid={!!formik.errors.properUse}
                onChange={formik.handleChange}
              />
              <Form.Control.Feedback type={"invalid"}>
                {formik.errors.properUse}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Дозування</Form.Label>
              <Form.Control
                as={"textarea"}
                name="dosing"
                defaultValue={formik.values.dosing}
                value={formik.values.dosing}
                onBlur={formik.handleBlur}
                isValid={formik.touched?.dosing && !formik.errors.dosing}
                isInvalid={!!formik.errors.dosing}
                onChange={formik.handleChange}
              />
              <Form.Control.Feedback type={"invalid"}>
                {formik.errors.dosing}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Взаємодії</Form.Label>
              <Form.Control
                as={"textarea"}
                name="interactions"
                defaultValue={formik.values.interactions}
                value={formik.values.interactions}
                onBlur={formik.handleBlur}
                isValid={
                  formik.touched?.interactions && !formik.errors.interactions
                }
                isInvalid={!!formik.errors.interactions}
                onChange={formik.handleChange}
              />
              <Form.Control.Feedback type={"invalid"}>
                {formik.errors.interactions}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Побічні реакції</Form.Label>
              <Form.Control
                as={"textarea"}
                name="sideEffects"
                defaultValue={formik.values.sideEffects}
                value={formik.values.sideEffects}
                onBlur={formik.handleBlur}
                isValid={
                  formik.touched?.sideEffects && !formik.errors.sideEffects
                }
                isInvalid={!!formik.errors.sideEffects}
                onChange={formik.handleChange}
              />
              <Form.Control.Feedback type={"invalid"}>
                {formik.errors.sideEffects}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Застереження</Form.Label>
              <Form.Control
                as={"textarea"}
                name="precautions"
                defaultValue={formik.values.precautions}
                value={formik.values.precautions}
                onBlur={formik.handleBlur}
                isValid={
                  formik.touched?.precautions && !formik.errors.precautions
                }
                isInvalid={!!formik.errors.precautions}
                onChange={formik.handleChange}
              />
              <Form.Control.Feedback type={"invalid"}>
                {formik.errors.precautions}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Правила зберігання</Form.Label>
              <Form.Control
                as={"textarea"}
                name="storage"
                defaultValue={formik.values.storage}
                value={formik.values.storage}
                onBlur={formik.handleBlur}
                isValid={formik.touched?.storage && !formik.errors.storage}
                isInvalid={!!formik.errors.storage}
                onChange={formik.handleChange}
              />
              <Form.Control.Feedback type={"invalid"}>
                {formik.errors.storage}
              </Form.Control.Feedback>
            </Form.Group>
            <Row className="justify-content-md-center" style={{ margin: 5 }}>
              <Col xs lg="2">
                <Button variant="outline-warning">Відмінити</Button>
              </Col>
              <Col xs lg="2">
                <Button type="submit">Зберегти</Button>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default InstructionModal;
