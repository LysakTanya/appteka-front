import { useEffect, useState } from "react";
import IMedicine from "./data/IMedicine";
import "bootstrap/dist/css/bootstrap.min.css";
import DataTable from "react-data-table-component";
import MedicineModal from "./MedicineModal";
import {
  BiCheckCircle,
  BiEditAlt,
  BiInfoCircle,
  BiXCircle,
} from "react-icons/bi";
import MedicineFormModal from "./MedicineFormModal";
import medicineService from "./data/medicineService";
import IUpdateMedicineModel from "./data/IUpdateMedicineModel";
import { toast } from "react-toastify";
import ICategory from "./data/ICategory";
import Toast from "../components/Toast";
import Table from "../components/Table";

interface IMedicineTableProps {
  categories: ICategory[];
  medicines: IMedicine[];
  loading: boolean;
}

const MedicineTable = (props: IMedicineTableProps) => {
  const [medicineModalVisible, setMedicineModalVisible] = useState(false);
  const [editMedicineModalVisible, setEditMedicineModalVisible] = useState(
    false
  );
  const [activeMedicine, setActiveMedicine] = useState<IMedicine>(
    {} as IMedicine
  );
  const [hasInstruction, setHasInstruction] = useState(false);

  const handleMedicineModalOpen = (rowData: IMedicine) => {
    setActiveMedicine(rowData);
    setMedicineModalVisible(true);
  };

  const handleEditMedicineModalOpen = (rowData: IMedicine) => {
    setActiveMedicine(rowData);
    setEditMedicineModalVisible(true);
  };

  const columns = [
    {
      name: "Код",
      selector: "code",
      sortable: false,
    },
    {
      name: "Назва",
      selector: "name",
      sortable: true,
      grow: 3
    },
    {
      name: "Кількість",
      selector: "quantity",
      sortable: true,
      right: true,
    },
    {
      name: "Міра",
      selector: "category",
      sortable: false,
      cell: (row: any) => {
        return <div>{row.category.measure}</div>;
      },
    },
    {
      name: "Ціна",
      selector: "price",
      sortable: true,
    },
    {
      name: "Категорія",
      selector: "category.name",
      sortable: true,
      cell: (row: any) => {
        return <div>{row.category.name}</div>;
      },
    },
    {
      name: "Актуальність",
      selector: "isActual",
      sortable: true,
      center: true,
      cell: (row: any) => {
        return row.isActual ? (
          <BiCheckCircle size="1.7rem" />
        ) : (
          <BiXCircle size="1.7rem" />
        );
      },
    },

    {
      cell: (row: any) => (
        <div>
          <BiEditAlt
            style={{ margin: 4 }}
            size="1.7rem"
            onClick={() => handleEditMedicineModalOpen(row)}
          />
          <BiInfoCircle
            style={{ margin: 4 }}
            size="1.7rem"
            onClick={() => handleMedicineModalOpen(row)}
          />
        </div>
      ),
      name: "",
      ignoreRowClick: true,
      button: true,
      left: true,
    },
  ];

  const handleMedicineModalClose = () => {
    setMedicineModalVisible(false);
  };

  const handleEditMedicineModalClose = () => {
    setEditMedicineModalVisible(false);
  };

  const handleUpdateMedicine = async (values: any) => {
    const medicine: IUpdateMedicineModel = {
      id: values.id,
      name: values.name,
      isActual: Boolean(values.isActual),
      quantity: values.quantity,
      code: values.code,
      price: values.price,
      categoryId: values.categoryId,
      instruction: hasInstruction
        ? {
            id: values.id,
            properUse: values.properUse,
            sideEffects: values.sideEffects,
            precautions: values.precautions,
            storage: values.storage,
            interactions: values.interactions,
            dosing: values.dosing,
          }
        : null,
    };

    try {
      let response = await medicineService.updateMedicine(medicine);
      toast.success(`Препарат '${response.data.name}' був успішно оновлений.`);
      handleEditMedicineModalClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  useEffect(() => {
    setHasInstruction(activeMedicine.instruction != null);
  }, [activeMedicine]);

  const changeHasInstruction = (hasInstruction: boolean) => {
    setHasInstruction(hasInstruction);
  };

  return (
    <>
      <Table
        columns={columns}
        onRowClicked={(rowData: any) => {
          handleMedicineModalOpen(rowData);
        }}
        data={props.medicines}
        title="Медичні препарати"
        loading={props.loading}
      />
      <MedicineModal
        visible={medicineModalVisible}
        data={activeMedicine}
        handleClose={handleMedicineModalClose}
      />
      <MedicineFormModal
        visible={editMedicineModalVisible}
        handleClose={handleEditMedicineModalClose}
        initialValue={activeMedicine}
        onSubmit={handleUpdateMedicine}
        hasInstruction={hasInstruction}
        changeHasInstruction={changeHasInstruction}
        categories={props.categories}
      />
      <Toast />
    </>
  );
};

export default MedicineTable;
