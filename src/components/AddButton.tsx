import React from "react";
import { Button } from "react-bootstrap";

const AddButton = ({ onClick }: any) => {
  return (
    <Button variant="success" onClick={onClick} style={{ margin: 10 }}>
      Додати
    </Button>
  );
};

export default AddButton;