import React from "react";
import {
  Button,
  Form,
  FormControl,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";
import loginService from "../../account/data/loginService";
import NavigationBarStyle from "./NavigationBarStyle";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import Role from "../../account/data/Role";

const NavigationBar = () => {
  const handleLogout = () => {
    confirmAlert({
      title: "Вихід",
      message: "Ви впевнені, що хочете вийти?",
      buttons: [
        {
          label: "Так",
          onClick: () => {
            loginService.logout();
            window.location.href = "/login";
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };
  const roleId = Number(localStorage.getItem("roleId"));
  return (
    <>
      {localStorage.getItem("token") && (
        <Navbar bg="dark" variant="dark" style={NavigationBarStyle.navbarStyle}>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Navbar.Brand href="/home">Appтека</Navbar.Brand>
              <Nav.Link href="/orders">Замовлення</Nav.Link>
              <Nav.Link href="/shifts">Графік</Nav.Link>
              <Nav.Link href="/customers">Клієнти</Nav.Link>
              {roleId !== Role.Pharmacist && (
                <>
                  <Nav.Link href="/storages">Склад</Nav.Link>
                  <Nav.Link href="/decommissions">Списання</Nav.Link>
                  <Nav.Link href="/supplies">Поставки</Nav.Link>
                </>
              )}
              {roleId === Role.Admin && (
                <NavDropdown title="Управління" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/admin/shifts">
                    Графік
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/admin/workers">
                    Працівники
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="/admin/medicines">
                    Препарати
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="/admin/storages">
                    Склади
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/admin/suppliers">
                    Поставники
                  </NavDropdown.Item>
                </NavDropdown>
              )}
            </Nav>
          </Navbar.Collapse>
          <Button variant="link" href="/account">
            {localStorage.getItem("name")} {localStorage.getItem("surname")}
          </Button>
          <Button
            variant="danger"
            onClick={() => {
              handleLogout();
            }}
          >
            Вийти
          </Button>
        </Navbar>
      )}
    </>
  );
};

export default NavigationBar;
