import { CSSProperties } from "react";

const NavigationBarStyle = {
  navbarStyle: {
    padding: 10,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  } as CSSProperties,
};

export default NavigationBarStyle;
