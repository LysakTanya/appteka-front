import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";

const Pagination = ({ itemsPerPage, totalItems, paginate }: any) => {
  const pageNumbers: number[] = [];
  for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav>
      <ul className="pagination">
        <li>
          <a className="page-link" onClick={() => {}}></a>
        </li>
        {pageNumbers.map((number, index) => (
          <li key={index} className="page-item">
            <a className="page-link" onClick={() => paginate(number)}>
              {number}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;
