import { Spinner } from "react-bootstrap";

const ProgressSpinner = () => {
  return (
    <>
      <Spinner animation={"border"}></Spinner><b style={{marginLeft: 6}}>Завантаження даних...</b>
    </>
  );
};

export default ProgressSpinner;