import DataTable, { IDataTableColumn } from "react-data-table-component";
import ProgressSpinner from "./ProgressSpinner";

interface ITableProps {
  loading: boolean;
  data: any[];
  columns: IDataTableColumn[];
  title: string;
  onRowClicked?: any;
}

const Table = (props: ITableProps) => {
  return (
    <>
      <DataTable
        data={props.data}
        columns={props.columns}
        highlightOnHover
        striped
        pagination={true}
        paginationPerPage={10}
        noDataComponent={<p>Немає даних.</p>}
        title={props.title}
        progressPending={props.loading}
        onRowClicked={props.onRowClicked}
        progressComponent={<ProgressSpinner />}
        responsive={true}
      />
    </>
  );
};

export default Table;
