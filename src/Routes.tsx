const DefaultRoute: string = "https://localhost:44345/";

const Routes = {
  medicineRoute: DefaultRoute + "api/medicines/",
  categoryRoute: DefaultRoute + "api/categories/",
  decommissionRoute: DefaultRoute + "api/decommissions/",
  orderRoute: DefaultRoute + "api/orders/",
  homeRoute: DefaultRoute + "api/home/",
  shiftRoute: DefaultRoute + "api/shifts/",
  loginRoute: DefaultRoute + "api/login/",
  storageRoute: DefaultRoute + "api/storages/",
  workerRoute: DefaultRoute + "api/workers/",
  supplierRoute: DefaultRoute + "api/suppliers/",
  supplyRoute: DefaultRoute + "api/supplies/",
  workerTypeRoute: DefaultRoute + "api/worker-types/",
  customersRoute: DefaultRoute + "api/customers/",
};

export default Routes;
