import moment from "moment";
import Table from "../components/Table";
import { BiCheckDouble, BiX, BiTrash } from "react-icons/bi";
import { toast } from "react-toastify";
import ISupply from "./data/ISupply";
import { Button, Modal } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import React, { useRef } from "react";
import { useReactToPrint } from "react-to-print";
import styles from "./styles/supply.module.css";
import DataTable from "react-data-table-component";

interface ISupplyDetailsModalProps {
  data: ISupply;
  visible: boolean;
  handleClose: () => void;
}

const SupplyDetailsModal = (props: ISupplyDetailsModalProps) => {
  const { data } = props;
  const componentToPrintRef = useRef<any>();
  const handlePrint = useReactToPrint({
    content: () => componentToPrintRef.current,
  });

  const columns = [
    {
      name: "Код товару",
      selector: "medicine.code",
      sortable: true,
    },
    {
      name: "Назва товару",
      selector: "medicine.name",
      sortable: true,
    },
    {
      name: "Категорія товару",
      selector: "medicine.category",
      sortable: true,
      cell: (row: any) => <div>{row.medicine?.category?.name}</div>,
    },
    {
      name: "Кількість/од",
      selector: "count",
      sortable: true,
    },
    {
      name: "Ціна",
      selector: "price",
      sortable: true,
      cell: (row: any) => <div>{row.price} грн</div>,
    },
    {
      name: "Сума",
      selector: "price",
      sortable: false,
      cell: (row: any) => <div>{row.price * row.count} грн</div>,
    },
  ];

  return (
    <>
      <div style={{ display: "none" }}>
        <div ref={componentToPrintRef} style={{margin: 10}}>
          <h4 style={{textAlign: 'center'}}>Поставка №{data.id}</h4>
          <p>
            <b>Дата:</b> {moment(data.date).format("MMM DD, YYYY")}
          </p>
          <p>
            <b>Поставник:</b> {data.supplier?.name} -{" "}
            {data.supplier?.phoneNumber}
          </p>
          <p>
            <b>Призначення поставки:</b> №{data.storage?.id} - м.{" "}
            {data.storage?.city}, вул. {data.storage?.street} буд.{" "}
            {data.storage?.number}
          </p>
          <DataTable title={"Товари"} columns={columns} data={data.medicines} />
          <h4 style={{margin: 20}}>Загальна сума: {data.price} грн</h4>
        </div>
      </div>

      <Modal
        size="xl"
        dialogClassName="large-modal"
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>Поставка №{data.id}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <p>
            <b>Дата:</b> {moment(data.date).format("MMM DD, YYYY")}
          </p>
          <p>
            <b>Поставник:</b> {data.supplier?.name} -{" "}
            {data.supplier?.phoneNumber}
          </p>
          <p>
            <b>Призначення поставки:</b> №{data.storage?.id} - м.{" "}
            {data.storage?.city}, вул. {data.storage?.street} буд.{" "}
            {data.storage?.number}
          </p>
          <Table
            title={"Товари"}
            columns={columns}
            loading={false}
            data={data.medicines}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={props.handleClose}>
            Закрити
          </Button>
          <Button variant="info" onClick={handlePrint}>
            Роздрукувати
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default SupplyDetailsModal;
