import React, { useEffect, useState } from "react";
import { Tabs, Tab, Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import ISupply from "./data/ISupply";
import supplyService from "./data/supplyService";
import SupplyTable from "./SupplyTable";
import styles from "./styles/supply.module.css";
import CreateNewSupply from "./CreateNewSupply";
import { AiFillMedicineBox } from "react-icons/ai";

const SupplyPage = () => {
  const [supplies, setSupplies] = useState<ISupply[]>([] as ISupply[]);
  const [loading, setLoading] = useState(false);
  const [createMode, setCreateMode] = useState(false);

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        setSupplies((await supplyService.getSupplies()).data);
      } catch (error) {
        toast.error(error.response.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  const leftCreateMode = () => setCreateMode(false);

  const FirstStep = () => {
    return (
      <div className={styles.firstStepContainer}>
        <h3>
          Створити нову поставку <AiFillMedicineBox size="4rem" />
        </h3>
        <Button
          onClick={() => {
            setCreateMode(true);
          }}
        >
          Перейти до створення
        </Button>
      </div>
    );
  };
  
  return (
    <>
      <Tabs defaultActiveKey="newSupply">
        <Tab eventKey="newSupply" title="Нова поставка">
          {createMode ? (
            <CreateNewSupply handleLeave={leftCreateMode} />
          ) : (
            <FirstStep />
          )}
        </Tab>
        <Tab eventKey="supplies" title="Поставки">
          <SupplyTable loading={loading} data={supplies} title="Поставки" />
        </Tab>
      </Tabs>
      <Toast />
    </>
  );
};

export default SupplyPage;
