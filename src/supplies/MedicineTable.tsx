import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import IMedicine from "../medicines/data/IMedicine";
import { BiAddToQueue } from "react-icons/bi";
import MedicineSupplyModal from "./MedicineSupplyModal";
import IMedicineSupply from "./data/IMedicineSupply";
import DataTable from "react-data-table-component";

interface IMedicineTableProps {
  medicines: IMedicine[];
  loading: boolean;
  handleAddMedicine: (medicineSupply: IMedicineSupply) => void;
}

const MedicineTable = (props: IMedicineTableProps) => {
  const [visible, setVisible] = useState(false);
  const handleClose = () => {
    setVisible(false);
  };
  const [activeMedicine, setActiveMedicine] = useState<IMedicine>(
    {} as IMedicine
  );

  const columns = [
    {
      name: "Код",
      selector: "code",
      sortable: false,
      left: true,
      compact: true,
      width: "20%",
    },
    {
      name: "Назва",
      selector: "name",
      sortable: true,
      left: true,
      compact: true,
      width: "35%",
    },
    {
      name: "К-сть",
      selector: "quantity",
      sortable: true,
      left: true,
      compact: true,
      width: "7%",
    },
    {
      name: "Міра",
      selector: "category",
      sortable: false,
      compact: true,
      left: true,
      width: "7%",
      cell: (row: any) => {
        return <div>{row.category?.measure}</div>;
      },
    },
    {
      name: "Ціна",
      selector: "price",
      sortable: true,
      compact: true,
      left: true,
      width: "10%",
    },
    {
      name: "Категорія",
      selector: "category.name",
      sortable: true,
      compact: true,
      left: true,
      width: "15%",
      cell: (row: any) => {
        return <div>{row.category?.name}</div>;
      },
    },
    {
      name: "",
      selector: "category",
      sortable: false,
      compact: true,
      right: true,
      width: "5%",
      cell: (row: any) => {
        if (!row.isActual) {
          return <></>;
        }

        return (
          <BiAddToQueue
            size="1.7rem"
            onClick={() => {
              setActiveMedicine(row);
              setVisible(true);
            }}
          />
        );
      },
    },
  ];

  return (
    <>
      <DataTable
        conditionalRowStyles={[
          {
            when: (row) => row.isActual === false,
            style: {
              backgroundColor: "grey",
              color: "white",
              "&:hover": {
                cursor: "cross",
              },
            },
          },
        ]}
        columns={columns}
        data={props.medicines}
        noHeader
        progressPending={props.loading}
        dense={true}
        pagination={true}
      />
      <MedicineSupplyModal
        medicine={activeMedicine}
        visible={visible}
        handleClose={handleClose}
        title={"Товар для поставки"}
        handleAddMedicine={props.handleAddMedicine}
      />
    </>
  );
};

export default MedicineTable;
