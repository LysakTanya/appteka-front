import IMedicine from "../../medicines/data/IMedicine";

interface IMedicineSupply {
  medicine: IMedicine;
  count: number;
  price: number;
}

export default IMedicineSupply;
