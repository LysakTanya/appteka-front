import IStorage from "../../storages/data/IStorage";
import ISupplier from "../../suppliers/data/ISupplier";
import IMedicineSupply from "./IMedicineSupply";

interface ISupply {
    id: number;
    date: Date;
    supplier: ISupplier;
    medicines: IMedicineSupply[];
    price: number;
    storage: IStorage;
}

export default ISupply;