import Axios from "../../Axios";
import Routes from "../../Routes";
import ICreateSupplyModel from "./ICreateSupplyModel";
import ISupply from "./ISupply";

const supplyService = {
  getSupplies: async () => {
    return Axios.get<ISupply[]>(Routes.supplyRoute);
  },

  addSupply: async (supply: ICreateSupplyModel) => {
    return Axios.post<ISupply>(Routes.supplyRoute, supply);
  },
};
export default supplyService;
