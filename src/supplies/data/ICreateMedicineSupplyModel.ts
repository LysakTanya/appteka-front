interface ICreateMedicineSupplyModel {
  medicineId: number;
  count: number;
  price: number;
}

export default ICreateMedicineSupplyModel;
