import ICreateMedicineSupplyModel from "./ICreateMedicineSupplyModel";

interface ICreateSupplyModel {
    supplierId: number;
    medicineSupplies: ICreateMedicineSupplyModel[];
    storageId: number;
}

export default ICreateSupplyModel;