import moment from "moment";
import Table from "../components/Table";
import ISupply from "./data/ISupply";
import SupplyDetailsModal from "./SupplyDetailsModal";
import { useState } from "react";

interface ISupplyTableProps {
  loading: boolean;
  data: ISupply[];
  title: string;
}

const SupplyTable = (props: ISupplyTableProps) => {
  const [visible, setVisible] = useState(false);
  const [activeSupply, setActiveSupply] = useState<ISupply>({} as ISupply);

  const columns = [
    {
      name: "Дата поставки",
      selector: "date",
      sortable: true,
      cell: (row: any) => <div>{moment(row.date).format("MMM DD, YYYY")}</div>,
    },
    {
      name: "Поставник",
      selector: "supplier.name",
      sortable: true,
    },
    {
      name: "Сума",
      selector: "price",
      sortable: true,
      cell: (row: any) => <div>{row.price} грн</div>,
    },
    {
      name: "Торгова точка",
      selector: "storage",
      cell: (row: any) => (
        <div>
          №{row.storage.id} - м. {row.storage.city}, вул. {row.storage.street}{" "}
          буд. {row.storage.number}
        </div>
      ),
    },
  ];

  const handleRowClick = (supply: ISupply) => {
    setActiveSupply(supply);
    setVisible(true);
  };

  const handleClose = () => {
    setVisible(false);
  };

  return (
    <>
      <Table
        title={props.title}
        columns={columns}
        loading={props.loading}
        data={props.data}
        onRowClicked={(row: any) => handleRowClick(row)}
      />
      <SupplyDetailsModal
        visible={visible}
        data={activeSupply}
        handleClose={handleClose}
      />
    </>
  );
};

export default SupplyTable;
