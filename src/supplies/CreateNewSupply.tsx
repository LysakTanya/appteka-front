import React, { useEffect, useState } from "react";
import {
  Tabs,
  Tab,
  Table,
  Alert,
  ListGroup,
  Container,
  Button,
  Form,
  Navbar,
  Nav,
} from "react-bootstrap";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import ISupplier from "../suppliers/data/ISupplier";
import ISupply from "./data/ISupply";
import supplyService from "./data/supplyService";
import SupplyTable from "./SupplyTable";
import styles from "./styles/supply.module.css";
import IMedicine from "../medicines/data/IMedicine";
import medicineService from "../medicines/data/medicineService";
import supplierService from "../suppliers/data/supplierService";
import storageService from "../storages/data/storageService";
import MedicineTable from "./MedicineTable";
import IStorage from "../storages/data/IStorage";
import IMedicineSupply from "./data/IMedicineSupply";
import ICreateSupplyModel from "./data/ICreateSupplyModel";
import MedicineSupplyTable from "./MedicineSupplyTable";
import { AiTwotoneAlert } from "react-icons/ai";
import { configure } from "@testing-library/dom";
import { confirmAlert } from "react-confirm-alert";
import SupplyDetailsModal from "./SupplyDetailsModal";

const CreateNewSupply = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [suppliers, setSuppliers] = useState<ISupplier[]>([] as ISupplier[]);
  const [medicines, setMedicines] = useState<IMedicine[]>([] as IMedicine[]);
  const storageId = Number(localStorage.getItem("storageId"));
  const [supplierId, setSupplierId] = useState<number>(0);
  const [medicineSupplies, setMedicineSupplies] = useState<IMedicineSupply[]>(
    [] as IMedicineSupply[]
  );
  const [filteredMedicines, setFilteredMedicines] = useState<IMedicine[]>(
    [] as IMedicine[]
  );
  const [visible, setVisible] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const handleSearch = () => {
    if (searchValue === "") {
      return;
    }

    const filteredMedicines = medicines.filter(
      (item) =>
        item.name.includes(searchValue) || item.code.includes(searchValue)
    );
    setFilteredMedicines(filteredMedicines);
  };

  const [newSupply, setNewSupply] = useState<ISupply>({} as ISupply);

  const handleAddMedicineToSupply = (inputMedicineSupply: IMedicineSupply) => {
    setMedicineSupplies((prev) => {
      const index = prev.findIndex(
        (item) => item.medicine?.id === inputMedicineSupply.medicine?.id
      );
      if (index !== -1) {
        const medicineSupply = prev[index];
        const newMedicineSupply = {
          medicine: medicineSupply.medicine,
          price: inputMedicineSupply.price,
          count: medicineSupply.count + inputMedicineSupply.count,
        };
        const filtered = prev.filter((item, i) => i !== index);
        return filtered.concat(newMedicineSupply);
      }

      return prev.concat(inputMedicineSupply);
    });
  };

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        setMedicines((await medicineService.getMedicines()).data);
        setSuppliers((await supplierService.getSuppliers()).data);
      } catch (error) {
        toast.error(error.response.data);
      }

      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    setSupplierId(suppliers[0]?.id);
  }, [suppliers]);

  useEffect(() => {
    setFilteredMedicines(medicines);
  }, [medicines]);

  const handleRemoveMedicineSupply = (medicineId: number) => {
    setMedicineSupplies((prev) => {
      return prev.filter((item) => item.medicine?.id !== medicineId);
    });
  };

  const handleAddSupply = (e: any) => {
    if(supplierId === 0){
      toast.error('Поставник не обраний.');
      return;
    }

    if(medicineSupplies == undefined || medicineSupplies.length === 0){
      toast.error('Поставка не може містити 0 товарів. Додайте хоча б один товар до поставки');
      return;
    }

    confirmAlert({
      message: "Ви впевнені, що хочете додати поставку?",
      title: "Нова поставка",
      buttons: [
        {
          label: "Так",
          onClick: () => {
            const newSupply: ICreateSupplyModel = {
              storageId: storageId,
              supplierId: supplierId,
              medicineSupplies: medicineSupplies.map((item) => {
                return {
                  price: item.price,
                  count: item.count,
                  medicineId: item.medicine.id,
                };
              }),
            };

            supplyService
              .addSupply(newSupply)
              .then((response) => {
                clearValues();
                toast.success("Поставка була успішно додана!");
                setNewSupply(response.data);
                setVisible(true);
              })
              .catch((error) => toast.error(error.response?.data));
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const handleSupplierChange = (e: any) => {
    setSupplierId(Number(e.target.value));
  };

  const clearValues = () => {
    setMedicineSupplies([]);
    setSearchValue("");
    setFilteredMedicines(medicines);
  };

  const handleReset = () => {
    confirmAlert({
      title: "Відміна",
      message: "Всі зміни будуть скинуті.",
      buttons: [
        {
          label: "Ок",
          onClick: () => {
            clearValues();
            props.handleLeave();
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const handleClose = () => {
    setVisible(false);
    props.handleLeave();
  };

  return (
    <>
      <SupplyDetailsModal
        data={newSupply}
        visible={visible}
        handleClose={handleClose}
      />
      <div className={styles.newSupplyContainer}>
        <div className={styles.newSupplySubcontainer}>
          <Navbar
            bg="primary"
            variant="dark"
            style={{ marginTop: 20, marginBottom: 5 }}
          >
            <Form className={styles.navForm}>
              <Form.Label
                style={{
                  flex: 1.5,
                  alignSelf: "center",
                  marginLeft: 5,
                  color: "white",
                  fontWeight: 700,
                }}
              >
                Пошук товарів
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                value={searchValue}
                style={{ margin: 5, flex: 4 }}
                onChange={(e: any) => setSearchValue(e.target.value)}
              />
              <Button
                variant="outline-light"
                style={{ margin: 5, flex: 1 }}
                onClick={handleSearch}
              >
                Пошук
              </Button>
              <Button
                variant="outline-warning"
                style={{ margin: 5, flex: 1 }}
                type="reset"
                onClick={() => {
                  setSearchValue("");
                  setFilteredMedicines(medicines);
                }}
              >
                Очистити
              </Button>
            </Form>
          </Navbar>
          <MedicineTable
            medicines={filteredMedicines}
            loading={loading}
            handleAddMedicine={handleAddMedicineToSupply}
          />
        </div>
        <div style={{ width: 2, backgroundColor: "black", margin: 15 }} />
        <div className={styles.newSupplySubcontainer}>
          <Container>
            <h3>Оформлення поставки</h3>
            <p>
              <b>Торгова точка призначення:</b> №{storageId}
            </p>
            <Form.Group>
              <Form.Label>
                <b>Поставник:</b>
              </Form.Label>
              <Form.Control as={"select"} onChange={handleSupplierChange}>
                {suppliers?.map((item) => (
                  <option value={item.id}>
                    {item.name} - {item.representative}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
          </Container>
          <hr />
          <MedicineSupplyTable
            medicineSupplies={medicineSupplies}
            handleRemoveMedicine={handleRemoveMedicineSupply}
          />
          <hr />
          <p>
            <b>Загальна сума: </b>
            {medicineSupplies?.reduce(
              (total, current) => total + current.count * current.price,
              0
            ) ?? 0}{" "}
            грн
          </p>
          <div className={styles.buttonContainer}>
            <Button
              variant="primary"
              onClick={handleAddSupply}
              style={{ margin: 10 }}
            >
              Створити
            </Button>
            <Button
              variant="warning"
              onClick={handleReset}
              style={{ margin: 10 }}
            >
              Відмінити
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateNewSupply;
