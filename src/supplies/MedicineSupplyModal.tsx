import { FormikErrors, useFormik } from "formik";
import { useEffect } from "react";
import { Modal, Button, Form, Alert } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import * as yup from "yup";
import IMedicine from "../medicines/data/IMedicine";
import IMedicineSupply from "./data/IMedicineSupply";

interface IMedicineSupplyModalProps {
  visible: boolean;
  handleClose: () => void;
  title: string;
  medicine: IMedicine;
  handleAddMedicine: (medicineSupply: IMedicineSupply) => void;
}

const MedicineSupplyModal = (props: IMedicineSupplyModalProps) => {
  const { medicine } = props;
  const schema = yup.object().shape({
    count: yup
      .number()
      .required("Кількість є обов'язковим полем. ")
      .moreThan(0, "Кількість має бути більшою за 0."),
    price: yup
      .number()
      .required("Ціна є обов'язковим полем. ")
      .moreThan(0, "Ціна має бути більшою за 0."),
  });

  let formik = useFormik({
    initialValues: {
      count: 10,
      price: 10,
    },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      const medicineSupply: IMedicineSupply = {
        medicine: medicine,
        count: values.count,
        price: values.price,
      };

      props.handleAddMedicine(medicineSupply);
      props.handleClose();
    },
  });

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Alert variant="success">
            <Alert.Heading>{medicine.code}</Alert.Heading>
            <p>
              {medicine.name} - {medicine.category?.name}
            </p>
            <hr />
            <p className="mb-0">
              {medicine.quantity} {medicine.category?.measure}
            </p>
          </Alert>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Ціна/грн</Form.Label>
              <Form.Control
                type="number"
                name="price"
                placeholder="Введіть ціну"
                defaultValue={formik.values.price}
                isValid={formik.touched?.price && !formik.errors.price}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.price}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.price}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Кількість/од</Form.Label>
              <Form.Control
                type="number"
                name="count"
                placeholder="Введіть кількість"
                defaultValue={formik.values.count}
                isValid={formik.touched?.count && !formik.errors.count}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.count}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.count}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default MedicineSupplyModal;
