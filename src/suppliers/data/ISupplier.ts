interface ISupplier {
  id: number;
  name: string;
  email: string;
  representative: string;
  city: string;
  phoneNumber: string;
}

export default ISupplier;
