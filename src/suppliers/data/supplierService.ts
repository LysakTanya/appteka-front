import Axios from "../../Axios";
import Routes from "../../Routes";
import ISupplier from "./ISupplier";

const supplierService = {
  getSuppliers: () => Axios.get<ISupplier[]>(Routes.supplierRoute),

  addSupplier: (supplier: ISupplier) =>
    Axios.post<ISupplier>(Routes.supplierRoute, supplier),

  updateSupplier: (supplier: ISupplier) =>
    Axios.put<ISupplier>(Routes.supplierRoute, supplier),
};

export default supplierService;
