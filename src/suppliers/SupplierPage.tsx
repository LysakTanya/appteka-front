import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import Toast from "../components/Toast";
import ISupplier from "./data/ISupplier";
import supplierService from "./data/supplierService";
import SupplierModal from "./SupplierModal";
import SupplierTable from "./SupplierTable";

const SupplierPage = () => {
  const [suppliers, setSuppliers] = useState<ISupplier[]>([] as ISupplier[]);
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchSuppliers = async () => {
      try {
        setLoading(true);
        let response = await supplierService.getSuppliers();
        setSuppliers(response.data);
        setLoading(false);
      } catch (error) {
        toast.error(error.response.data);
        setLoading(false);
      }
    };

    fetchSuppliers();
  }, []);

  const handleClose = () => {
    setVisible(false);
  };

  const handleAddClick = () => {
    setVisible(true);
  };

  const handleAddSupplier = async (values: any) => {
    const supplier: ISupplier = {
      id: 0,
      name: values.name,
      city: values.city,
      email: values.email,
      phoneNumber: values.phoneNumber,
      representative: values.representative,
    };
    console.log({ supplier: supplier });
    try {
      let response = await supplierService.addSupplier(supplier);
      toast.success(`Поставник ${response.data.name} був успішно доданий.`);
      handleClose();
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <Button variant="success" onClick={handleAddClick}>
        Додати
      </Button>
      <SupplierTable data={suppliers} loading={loading} />
      <SupplierModal
        initialValue={{} as ISupplier}
        title={"Новий поставник"}
        handleClose={handleClose}
        onSubmit={handleAddSupplier}
        visible={visible}
      />
      <Toast />
    </>
  );
};

export default SupplierPage;
