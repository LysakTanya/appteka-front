import React from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { FormikErrors, useFormik } from "formik";
import { useEffect } from "react";
import ISupplier from "./data/ISupplier";

interface ISupplierModalProps {
  handleClose: () => void;
  visible: boolean;
  initialValue: ISupplier;
  onSubmit: (values: any) => Promise<void>;
  title: string;
}

const SupplierModal = (props: ISupplierModalProps) => {
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Назва є обов'язковим полем. ")
      .min(2, "Назва має містити мінімум 2 символи. ")
      .max(50, "Назва може містити максимум 50 символів."),
    email: yup
      .string()
      .min(2, "Електронна пошта має містити мінімум 2 символи. ")
      .max(50, "Електронна пошта може містити максимум 50 символів.")
      .email("Введіть коректну електронну пошту.")
      .required("Електронна пошта є обов'язковим полем. "),
    representative: yup
      .string()
      .min(2, "Представник має містити мінімум 2 символи. ")
      .max(200, "Представник може містити максимум 200 символів.")
      .required("Представник є обов'язковим полем. "),
    phoneNumber: yup
      .string()
      .matches(new RegExp("^[+]{1}[0-9]{12}$"), "Некоректний номер телефону")
      .required("Номер телефону є обов'язковим полем. "),
    city: yup
      .string()
      .min(2, "Місто має містити мінімум 2 символи. ")
      .max(50, "Місто може містити максимум 50 символів.")
      .required("Місто є обов'язковим полем. "),
  });

  let formik = useFormik({
    initialValues: { ...props.initialValue },
    validationSchema: schema,
    onSubmit: props.onSubmit,
  });

  useEffect(() => {
    formik.setValues({ ...props.initialValue }, false);
  }, [props.initialValue]);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={props.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX onClick={props.handleClose} />
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={formik.handleSubmit} style={{ margin: 10 }}>
            <Form.Group>
              <Form.Label>Назва</Form.Label>
              <Form.Control
                type="text"
                name="name"
                max={50}
                min={2}
                placeholder="Введіть назву"
                defaultValue={formik.values.name}
                value={formik.values.name}
                isValid={formik.touched?.name && !formik.errors.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.name}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.name}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Електронна пошта</Form.Label>
              <Form.Control
                type="email"
                name="email"
                max={50}
                min={2}
                placeholder="Введіть електронну пошту"
                defaultValue={formik.values.email}
                value={formik.values.email}
                isValid={formik.touched?.email && !formik.errors.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Номер телефону</Form.Label>
              <Form.Control
                type="phone"
                name="phoneNumber"
                max={13}
                placeholder="Введіть номер телефону"
                defaultValue={formik.values.phoneNumber}
                value={formik.values.phoneNumber}
                isValid={
                  formik.touched?.phoneNumber && !formik.errors.phoneNumber
                }
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.phoneNumber}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.phoneNumber}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Місто</Form.Label>
              <Form.Control
                type="text"
                name="city"
                max={50}
                min={2}
                placeholder="Введіть назву міста"
                defaultValue={formik.values.city}
                value={formik.values.city}
                isValid={formik.touched?.city && !formik.errors.city}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.city}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.city}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Представник</Form.Label>
              <Form.Control
                type="text"
                name="representative"
                max={200}
                min={2}
                placeholder="Введіть ім’я представника"
                defaultValue={formik.values.representative}
                value={formik.values.representative}
                isValid={
                  formik.touched?.representative &&
                  !formik.errors.representative
                }
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={!!formik.errors.representative}
              />
              <Form.Control.Feedback type="invalid">
                {formik.errors.representative}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={props.handleClose}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default SupplierModal;
