import { useState } from "react";
import { BiEditAlt } from "react-icons/bi";
import { toast } from "react-toastify";
import Table from "../components/Table";
import Toast from "../components/Toast";
import ISupplier from "./data/ISupplier";
import supplierService from "./data/supplierService";
import SupplierModal from "./SupplierModal";

interface ISupplierTableProps {
  data: ISupplier[];
  loading: boolean;
}

const SupplierTable = (props: ISupplierTableProps) => {
  const [visible, setVisible] = useState(false);
  const [activeSupplier, setActiveSupplier] = useState<ISupplier>(
    {} as ISupplier
  );
  const handleClose = () => {
    setVisible(false);
  };

  const handleEditClick = (row: any) => {
    const supplier: ISupplier = {
      id: row.id,
      name: row.name,
      representative: row.representative,
      email: row.email,
      phoneNumber: row.phoneNumber,
      city: row.city,
    };
    setActiveSupplier(supplier);
    setVisible(true);
  };

  const columns = [
    {
      name: "Назва",
      selector: "name",
      sortable: true,
    },
    {
      name: "Представник",
      selector: "representative",
      sortable: true,
    },
    {
      name: "Номер телефону",
      selector: "phoneNumber",
      sortable: true,
    },
    {
      name: "Електронна пошта",
      selector: "email",
      sortable: true,
    },
    {
      name: "Місто",
      selector: "city",
      sortable: true,
    },
    {
      name: "",
      selector: "id",
      sortable: false,
      right: true,
      cell: (row: any) => {
        return <BiEditAlt size="1.7rem" onClick={() => handleEditClick(row)} />;
      },
    },
  ];

  const handleSupplierUpdate = async (values: any) => {
    const supplier: ISupplier = {
      id: values.id,
      name: values.name,
      city: values.city,
      representative: values.representative,
      phoneNumber: values.phoneNumber,
      email: values.email,
    };

    try {
      let response = await supplierService.updateSupplier(supplier);
      toast.success(`Поставник ${response.data.name} був успішно оновлений.`);
    } catch (error) {
      toast.error(error.response.data);
    }
  };

  return (
    <>
      <Table
        data={props.data}
        loading={props.loading}
        title="Поставники"
        columns={columns}
      />
      <SupplierModal
        title="Оновити дані поставника"
        handleClose={handleClose}
        initialValue={activeSupplier}
        visible={visible}
        onSubmit={handleSupplierUpdate}
      />
      <Toast />
    </>
  );
};

export default SupplierTable;
