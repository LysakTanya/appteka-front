import IShift from "./IShift";
import Routes from "../../Routes";
import Axios from "../../Axios";
import IWorker from "../../workers/data/IWorker";
import ICreateShiftModel from "./ICreateShiftModel";

const shiftService = {
  getShiftsForCurrentWeek: async () =>
    Axios.get<IShift[]>(Routes.shiftRoute + "currentWeek"),

  getShiftsForWeek: async (weekOffset: number) =>
    Axios.get<IShift[]>(`${Routes.shiftRoute}weeks/${weekOffset}`),

  getShiftsForWorker: async (weekOffset: number, workerId: number) =>
    Axios.get<IShift[]>(
      `${Routes.shiftRoute}weeks/${weekOffset}/workers/${workerId}`
    ),

  getAvailableWorkers: async (
    startDate: Date,
    hours: number,
    storageId: number
  ) =>
    Axios.get<IWorker[]>(
      `${
        Routes.shiftRoute
      }start/${startDate.toDateString()}/duration/${hours}/storages/${storageId}`
    ),

  addShift: async (shift: ICreateShiftModel) =>
    Axios.post<IShift>(Routes.shiftRoute, shift),

  deleteShift: async (shiftId: number) =>
    Axios.delete(`${Routes.shiftRoute}${shiftId}`),
};

export default shiftService;
