import IWorker from "../../workers/data/IWorker";

interface IShift {
  id: number;
  startDate: Date;
  endDate: Date;
  isDouble: boolean;
  worker: IWorker;
}

export default IShift;
