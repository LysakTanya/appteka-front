interface ICreateShiftModel {
  startDate: Date;
  endDate: Date;
  isDouble: boolean;
  workerId: number;
}

export default ICreateShiftModel;