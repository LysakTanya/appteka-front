import moment from "moment";
import Table from "../components/Table";
import IShift from "./data/IShift";
import { BiCheckDouble, BiX, BiTrash } from "react-icons/bi";
import { confirmAlert } from "react-confirm-alert";
import shiftService from "./data/shiftService";
import { toast } from "react-toastify";

interface IShiftTableProps {
  loading: boolean;
  data: IShift[];
  title: string;
}

const ShiftTable = (props: IShiftTableProps) => {
  const handleDelete = (id: number) => {
    confirmAlert({
      title: "Видалення",
      message: "Ви впевнені, що хочете видалити зміну?",
      buttons: [
        {
          label: "Так",
          onClick: () => {
            shiftService
              .deleteShift(id)
              .then((response) => {
                toast.success("Успішно видалено.");
                setTimeout(() => window.location.reload(), 1000);
              })
              .catch((error) => {
                toast.error(error.response?.data);
              });
          },
        },
        {
          label: "Відмінити",
          onClick: () => {},
        },
      ],
    });
  };

  const columns = [
    {
      name: "Початок зміни",
      selector: "startDate",
      sortable: true,
      cell: (row: any) => (
        <div>{moment(row.startDate).format("MMM DD, YYYY - HH:mm a")}</div>
      ),
    },
    {
      name: "Кінець зміни",
      selector: "endDate",
      sortable: true,
      cell: (row: any) => (
        <div>{moment(row.endDate).format("MMM DD, YYYY - HH:mm a")}</div>
      ),
    },
    {
      name: "Зміна/год",
      selector: "startDate",
      center: true,
      cell: (row: any) => (
        <div>{moment(row.endDate).diff(moment(row.startDate), "hours")}</div>
      ),
    },
    {
      name: "Подвійна оплата",
      selector: "isDouble",
      center: true,
      cell: (row: any) => (
        <div>
          {row.isDouble ? (
            <BiCheckDouble size="1.7rem" />
          ) : (
            <BiX size="1.7rem" />
          )}
        </div>
      ),
    },
    {
      name: "Працівник",
      selector: "worker",
      cell: (row: any) => (
        <div>
          {row.worker.name} {row.worker.surname}
        </div>
      ),
    },
    {
      name: "Торгова точка",
      selector: "worker.storageId",
    },
    {
      name: "",
      selector: "worker",
      right: true,
      cell: (row: any) => (
        <div>
          {moment() < moment(row.endDate) && (
            <BiTrash size="1.7rem" onClick={() => handleDelete(row.id)} />
          )}
        </div>
      ),
    },
  ];

  return (
    <Table
      title={props.title}
      columns={columns}
      loading={props.loading}
      data={props.data}
    />
  );
};

export default ShiftTable;
