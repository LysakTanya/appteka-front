import React, { useState, useEffect } from "react";
import { Alert, Button, Dropdown, Form, InputGroup } from "react-bootstrap";
import IShift from "./data/IShift";
import shiftService from "./data/shiftService";
import { toast } from "react-toastify";
import ShiftTable from "./ShiftTable";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import moment from "moment";
import AddButton from "../components/AddButton";
import ShiftModal from "./ShiftModal";
import IStorage from "../storages/data/IStorage";
import storageService from "../storages/data/storageService";

const AdminSchedulePage = () => {
  const [shifts, setShifts] = useState<IShift[]>([] as IShift[]);
  const [week, setWeek] = useState(0);
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [storage, setStorage] = useState(
    Number(localStorage.getItem("storageId"))
  );
  const [storages, setStorages] = useState<IStorage[]>([] as IStorage[]);
  const [filteredShifts, setFilteredShifts] = useState<IShift[]>(
    [] as IShift[]
  );

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        let response = await storageService.getAllStorages();
        setStorages(response.data);
        setLoading(false);
      } catch (error) {
        toast.error(error.response.data);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        let response = await shiftService.getShiftsForWeek(week);
        setShifts(response.data);
        setLoading(false);
      } catch (error) {
        toast.error(error.response.data);
        setLoading(false);
      }
    };

    fetchData();
  }, [week]);

  const handleAddShift = (shift: IShift) => {
    setShifts((prev) => prev.concat(shift));
  };

  const getDateOfWeekStart = () => {
    const now = moment();
    const dayOfWeek = now.weekday();
    return now.add(week * 7 - dayOfWeek + 1, "days");
  };

  const getDateOfWeekEnd = () => {
    const now = moment();
    const dayOfWeek = now.weekday();
    return now.add(7 - dayOfWeek + week * 7, "days");
  };

  useEffect(() => {
    const filtered = shifts.filter(
      (item) => item.worker?.storageId === Number(storage)
    );
    setFilteredShifts(filtered);
  }, [shifts]);

  const handleStorageChange = (e: any) => {
    const filtered = shifts.filter(
      (item) => item.worker.storageId === Number(e.target.value)
    );
    setFilteredShifts(filtered);
    setStorage(e.target.value);
  };

  return (
    <div>
      <div>
        <AddButton onClick={() => setVisible(true)} />
      </div>
      <Alert
        className="mb-3"
        variant="primary"
        style={{
          display: "flex",
          textAlign: "center",
          justifyContent: "space-between",
        }}
      >
        <AiOutlineArrowLeft
          size="2rem"
          onClick={() => setWeek((prev) => prev - 1)}
        />
        <h4 style={{ textAlign: "center" }}>
          {getDateOfWeekStart().format("MMM DD, YYYY")} -{" "}
          {getDateOfWeekEnd().format("MMM DD, YYYY")}
        </h4>
        <AiOutlineArrowRight
          size="2rem"
          onClick={() => setWeek((prev) => prev + 1)}
        />
      </Alert>
      <Form.Group style={{ width: "400px" }}>
        <Form.Label>Поточна торгова точка</Form.Label>
        <Form.Control as="select" onChange={handleStorageChange}>
          {storages.map((item) => {
            return (
              <option value={item.id} selected={item.id == storage}>
                №{item.id} - м. {item.city}, вул. {item.street} буд.{" "}
                {item.number}
              </option>
            );
          })}
        </Form.Control>
      </Form.Group>
      <ShiftTable loading={loading} data={filteredShifts} title="Зміни" />
      <ShiftModal
        handleAddShift={handleAddShift}
        visible={visible}
        title="Додати зміну"
        handleClose={() => setVisible(false)}
        currentStorage={storage}
      />
    </div>
  );
};

export default AdminSchedulePage;
