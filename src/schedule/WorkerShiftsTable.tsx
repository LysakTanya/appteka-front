import moment from "moment";
import Table from "../components/Table";
import IShift from "./data/IShift";
import { BiCheckDouble, BiX } from "react-icons/bi";

interface IShiftTableProps {
  loading: boolean;
  data: IShift[];
  title: string;
}

const WorkerShiftsTable = (props: IShiftTableProps) => {
  const columns = [
    {
      name: "Початок зміни",
      selector: "startDate",
      sortable: true,
      cell: (row: any) => (
        <div>{moment(row.startDate).format("MMM DD, YYYY - HH:mm a")}</div>
      ),
    },
    {
      name: "Кінець зміни",
      selector: "endDate",
      sortable: true,
      cell: (row: any) => (
        <div>{moment(row.endDate).format("MMM DD, YYYY - HH:mm a")}</div>
      ),
    },
    {
      name: "Зміна/год",
      selector: "startDate",
      center: true,
      cell: (row: any) => (
        <div>{moment(row.endDate).diff(moment(row.startDate), "hours")}</div>
      ),
    },
    {
      name: "Подвійна оплата",
      selector: "isDouble",
      center: true,
      cell: (row: any) => (
        <div>
          {row.isDouble ? (
            <BiCheckDouble size="1.7rem" />
          ) : (
            <BiX size="1.7rem" />
          )}
        </div>
      ),
    },
  ];

  return (
    <Table
      title={props.title}
      columns={columns}
      loading={props.loading}
      data={props.data}
    />
  );
};

export default WorkerShiftsTable;
