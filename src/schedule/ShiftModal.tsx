import React, { createRef, useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { BsX } from "react-icons/bs";
import Toast from "../components/Toast";
import * as yup from "yup";
import { useFormik } from "formik";
import { useEffect } from "react";
import IShift from "./data/IShift";
import DatePicker from "react-datetime";
import moment from "moment";
import "react-datetime/css/react-datetime.css";
import ICreateShiftModel from "./data/ICreateShiftModel";
import shiftService from "./data/shiftService";
import IWorker from "../workers/data/IWorker";
import { toast } from "react-toastify";

interface IShiftModalProps {
  handleClose: () => void;
  visible: boolean;
  title: string;
  currentStorage: number;
  handleAddShift: (shift: IShift) => void;
}

const ShiftModal = (props: IShiftModalProps) => {
  const schema = yup.object().shape({
    workerId: yup.number().required("Працівник є обов'язковим полем. "),
  });
  const formRef = createRef<HTMLFormElement>();
  const [hours, setHours] = useState(8);
  const [startDate, setStartDate] = useState(new Date());
  const [availableWorkers, setAvailableWorkers] = useState<IWorker[]>(
    [] as IWorker[]
  );
  const [isDouble, setIsDouble] = useState(false);
  const [workerId, setWorkerId] = useState(0);

  let formik = useFormik({
    initialValues: { workerId: 0 },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      if (startDate <= new Date()) {
        toast.error("Дата старту не може бути менша за поточну.");
        return;
      }

      if(workerId === 0){
        toast.error("Заповніть поле працівника.");
        return;
      };

      const shift: ICreateShiftModel = {
        startDate: startDate,
        endDate: moment(startDate).add(hours, "hours").toDate(),
        workerId: Number(values.workerId),
        isDouble: isDouble,
      };

      try {
        const response = await shiftService.addShift(shift);
        toast.success('Зміна була успішно додана.');
        props.handleAddShift(response.data);
      } catch (error) {
        toast.error(error.response.data);
      }
      props.handleClose();
      clearFormAndData();
    },
  });

  const clearFormAndData = () => {
    setWorkerId(0);
    setIsDouble(false);
    setStartDate(new Date());
    setAvailableWorkers([] as IWorker[]);
    setHours(8);
    formik.resetForm();
  };

  useEffect(() => {
    if (!props.visible) return;

    const fetchWorkers = async () => {
      try {
        let response = await shiftService.getAvailableWorkers(
          startDate,
          hours,
          props.currentStorage
        );
        setAvailableWorkers(response.data);
        if (response.data.length === 0) {
          formik.setFieldValue("workerId", 0);
          toast.info("Нема доступних працівників для поточного часу.");
          return;
        } else {
          setWorkerId(response.data[0].id);
          formik.setFieldValue("workerId", response.data[0].id);
        }
      } catch (error) {
        toast.error(error.response?.data);
      }
    };

    fetchWorkers();
  }, [hours, startDate, props.currentStorage, props.visible]);

  return (
    <>
      <Modal
        show={props.visible}
        onHide={() => {
          props.handleClose();
          clearFormAndData();
        }}
        backdrop="static"
        keyboard={false}
        ref={formRef}
      >
        <Modal.Header>
          <Modal.Title>{props.title}</Modal.Title>
          <BsX
            onClick={() => {
              props.handleClose();
              clearFormAndData();
            }}
          />
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={formik.handleSubmit}
            style={{ margin: 10 }}
            onReset={formik.handleReset}
          >
            <Form.Group>
              <Form.Label>Час початку зміни</Form.Label>
              <DatePicker
                inputProps={{
                  style: { width: 250 },
                }}
                dateFormat="DD-MM-YYYY"
                timeFormat="hh:mm A"
                value={moment(startDate)}
                onChange={(e) => setStartDate(moment(e).toDate())}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Тривалість зміни</Form.Label>
              <Form.Control
                as="select"
                value={hours}
                onChange={(e) => setHours(Number(e.target.value))}
              >
                <option value={4}>4</option>
                <option value={8} selected={true}>
                  8
                </option>
                <option value={12}>12</option>
              </Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Працівник</Form.Label>
              <Form.Control
                as="select"
                name="workerId"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              >
                {availableWorkers.map((item) => (
                  <option value={item.id}>
                    {item.name} {item.surname}
                  </option>
                ))}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                {formik.errors.workerId}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Check
                type="checkbox"
                label={`подвійна оплата`}
                name="isDouble"
                defaultChecked={false}
                checked={isDouble}
                onChange={(e: any) => setIsDouble(e.target.checked)}
              />
            </Form.Group>
            <Button
              variant="secondary"
              style={{ margin: 10 }}
              onClick={() => {
                props.handleClose();
                clearFormAndData();
              }}
            >
              Відмінити
            </Button>
            <Button variant="primary" style={{ margin: 10 }} type="submit">
              Зберегти
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <Toast />
    </>
  );
};

export default ShiftModal;
