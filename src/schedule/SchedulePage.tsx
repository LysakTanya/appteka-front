import React, { useState, useEffect } from "react";
import { Alert, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import ShiftTable from "./ShiftTable";
import IShift from "./data/IShift";
import { toast } from "react-toastify";
import shiftService from "./data/shiftService";
import moment from "moment";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import WorkerShiftsTable from "./WorkerShiftsTable";
import Toast from "../components/Toast";

const SchedulePage = () => {
  const [shifts, setShifts] = useState<IShift[]>([] as IShift[]);
  const workerId = localStorage.getItem("id");
  const [week, setWeek] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await shiftService.getShiftsForWorker(week, Number(workerId));
        setShifts(response.data);
      } catch (error) {
        toast.error(error.response.data);
      }

      setLoading(false);
    };

    fetchData();
  }, [week]);

  const getDateOfWeekStart = () => {
    const now = moment();
    const dayOfWeek = now.weekday();
    return now.add(week * 7 - dayOfWeek + 1, "days");
  };

  const getDateOfWeekEnd = () => {
    const now = moment();
    const dayOfWeek = now.weekday();
    return now.add(7 - dayOfWeek + week * 7, "days");
  };

  return (
    <div>
      <Alert
        className="mb-3"
        variant="primary"
        style={{
          display: "flex",
          textAlign: "center",
          justifyContent: "space-between",
        }}
      >
        <AiOutlineArrowLeft
          size="2rem"
          onClick={() => setWeek((prev) => prev - 1)}
        />
        <h4 style={{ textAlign: "center" }}>
          {getDateOfWeekStart().format("MMM DD, YYYY")} -{" "}
          {getDateOfWeekEnd().format("MMM DD, YYYY")}
        </h4>
        <AiOutlineArrowRight
          size="2rem"
          onClick={() => setWeek((prev) => prev + 1)}
        />
      </Alert>
      <WorkerShiftsTable loading={loading} data={shifts} title="Зміни" />
      <Toast/>
    </div>
  );
};

export default SchedulePage;
