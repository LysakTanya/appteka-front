import axios, { AxiosRequestConfig } from "axios";

const Axios = axios.create({ timeout: 25000 });
Axios.interceptors.request.use((req: any) => {
  const token = localStorage.getItem("token");
  req.headers.Authorization = `Bearer ${token}`;
  return req;
});

axios.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.status === 401) {
      window.location.href = "/login";
    }
    if (error.response.status === 403) {
      window.location.href = "/403";
    }
    return Promise.reject(error);
  }
);

export default Axios;
