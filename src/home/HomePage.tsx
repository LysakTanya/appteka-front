import React, { useState, useEffect } from "react";
import medicineService from "../medicines/data/medicineService";
import Medicine from "../medicines/data/IMedicine";
import {
  Button,
  Card,
  Jumbotron,
  ListGroup,
  ListGroupItem,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import NavigationBar from "../components/nav-bar/NavigationBar";

const HomePage = () => {
  return (
    <>
      <Jumbotron className="text-center mb-auto">
        <h1>Вітаємо!</h1>
        <p>
          Ви успішно авторизувались. Тепер ви можете перейти до роботи.
          Перейдіть на відповідну вкладку, щоб розпочати ваші дії.
        </p>
        <p>
          <Button variant="primary" href="/orders">Перейти до замовлень</Button>
        </p>
      </Jumbotron>
    </>
  );
};

export default HomePage;
