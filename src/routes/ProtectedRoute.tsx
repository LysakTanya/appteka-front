import { Route, Redirect } from "react-router-dom";

function ProtectedRoute({ component: Component, ...rest }: any) {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (
          localStorage.getItem("token") == null ||
          localStorage.getItem("roleId") == null
        ) {
          return (
            <Redirect to={{ pathname: "/", state: { from: props.location } }} />
          );
        } else return <Component />;
      }}
    />
  );
}

export default ProtectedRoute;
