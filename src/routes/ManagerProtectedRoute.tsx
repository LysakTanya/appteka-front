import { Route, Redirect } from "react-router-dom";
import Role from "../account/data/Role";

function ManagerProtectedRoute({ component: Component, ...rest }: any) {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (localStorage.getItem("token") == null) {
          return (
            <Redirect to={{ pathname: "/", state: { from: props.location } }} />
          );
        } else if (localStorage.getItem("roleId") !== Role.Manager.toString() && localStorage.getItem("roleId") !== Role.Admin.toString()) {
          return (
            <Redirect
              to={{ pathname: "/home", state: { from: props.location } }}
            />
          );
        } else return <Component />;
      }}
    />
  );
}

export default ManagerProtectedRoute;
