import React, { useState } from "react";
import { Form, Button, Card, Row, Col, Container } from "react-bootstrap";
import { toast, ToastContainer } from "react-toastify";
import IUser from "./data/IUser";
import loginService from "./data/loginService";

const LoginPage = () => {
  const [user, setUser] = useState<IUser>({ email: "", password: "" });
  const [validated, setValidated] = useState(false);

  const handleSubmit = async (e: any) => {
    e.stopPropagation();
    e.preventDefault();
    setValidated(true);
    if (e.currentTarget.checkValidity() === false) {
      return;
    }

    await loginService
      .login(user)
      .then((response) => {
        window.location.href = "/home";
        toast.success("Вхід успішний");
      })
      .catch((error) => {
        toast.error("Пароль або пошта неправильні.");
      });
  };

  return (
    <>
      <Container>
        <Row className="justify-content-md-center" style={{margin: 50}}>
          <Col>
          <h1>Appтека</h1>
            <Card style={{ margin: 5}}>
              <Card.Header>Вхід</Card.Header>
              <Card.Body>
                <Card.Title>Будь ласка, увійдіть до системи.</Card.Title>
                <Form noValidate onSubmit={handleSubmit} validated={validated}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Електронна пошта</Form.Label>
                    <Form.Control
                      type="email"
                      required
                      placeholder="Enter email"
                      onChange={(e: any) => {
                        setUser((prev) => {
                          return { ...prev, email: e.target.value };
                        });
                      }}
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicPassword">
                    <Form.Label>Пароль</Form.Label>
                    <Form.Control
                      type="password"
                      required
                      placeholder="Password"
                      onChange={(e: any) => {
                        setUser((prev) => {
                          return { ...prev, password: e.target.value };
                        });
                      }}
                    />
                  </Form.Group>
                  <Button
                    variant="primary"
                    type="submit"
                    style={{ margin: 10 }}
                  >
                    Увійти
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </Container>
    </>
  );
};

export default LoginPage;
