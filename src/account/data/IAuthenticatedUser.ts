import Role from "./Role";

interface IAuthenticatedUser {
  id: number;
  email: string;
  name: string;
  surname: string;
  token: string;
  role: string;
  roleId: Role;
  birthDate: Date;
  hireDate: Date;
  storageId: number;
}

export default IAuthenticatedUser;
