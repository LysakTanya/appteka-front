import moment from "moment";
import Axios from "../../Axios";
import Routes from "../../Routes";
import IAuthenticatedUser from "./IAuthenticatedUser";
import IUser from "./IUser";

const loginService = {
  login: async (user: IUser) => {
    let { data } = await Axios.post<IAuthenticatedUser>(
      Routes.loginRoute,
      user
    );
    localStorage.setItem("id", String(data.id));
    localStorage.setItem("token", data.token);
    localStorage.setItem("name", data.name);
    localStorage.setItem("surname", data.surname);
    localStorage.setItem("role", data.role);
    localStorage.setItem("roleId", data.roleId.toString());
    localStorage.setItem("storageId", data.storageId.toString());
    localStorage.setItem("birthDate", moment(data.birthDate).format('MMM DD, YYYY'));
    localStorage.setItem("hireDate", moment(data.hireDate).format('MMM DD, YYYY'));
    localStorage.setItem("email", data.email);
  },

  logout: async () => {
    localStorage.removeItem("id");
    localStorage.removeItem("token");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    localStorage.removeItem("role");
    localStorage.removeItem("roleId");
    localStorage.removeItem("birthDate");
    localStorage.removeItem("hireDate");
    localStorage.removeItem("email");
    localStorage.removeItem("storageId");
  },
};

export default loginService;
