import React from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";

const AccountPage = () => {
  const user = {
    name: localStorage.getItem("name"),
    surname: localStorage.getItem("surname"),
    role: localStorage.getItem("role"),
    hireDate: localStorage.getItem("hireDate"),
    birthDate: localStorage.getItem("birthDate"),
    email: localStorage.getItem('email')
  };

  return (
    <div style={{ justifyContent: "center" }}>
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src="https://i.ibb.co/zbZcq2y/image.png" />
        <Card.Body>
          <Card.Title>Профіль</Card.Title>
          <Card.Text>
            Ви увійшли як {user.name} {user.surname}.
          </Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>Дата початку роботи: {user.hireDate}</ListGroupItem>
          <ListGroupItem>Дата народження: {user.birthDate}</ListGroupItem>
          <ListGroupItem>Статус: {user.role}</ListGroupItem>
          <ListGroupItem>Електронна пошта: {user.email}</ListGroupItem>
        </ListGroup>
        <Card.Body>
          <Card.Link href="#">Редагувати</Card.Link>
        </Card.Body>
      </Card>
    </div>
  );
};

export default AccountPage;
